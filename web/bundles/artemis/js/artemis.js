var nodiac = {'á': 'a', 'č': 'c', 'ď': 'd', 'é': 'e', 'ě': 'e', 'í': 'i', 'ň': 'n', 'ó': 'o', 'ř': 'r', 'š': 's', 'ť': 't', 'ú': 'u', 'ů': 'u', 'ý': 'y', 'ž': 'z'};

function make_url(s) {
    s = s.toLowerCase();
    var s2 = '';
    for (var i=0; i < s.length; i++) {
        s2 += (typeof nodiac[s.charAt(i)] != 'undefined' ? nodiac[s.charAt(i)] : s.charAt(i));
    }
    return s2.replace(/[^a-z0-9]+/g, '-').replace(/^-|-$/g, '');
}
$('document').ready(function() {
    $('.dropdown-toggle').dropdown();

    if($('input[name="form[slug]"]').length > 0)
        $('input[name="form[title]"]').keyup(function() {
            $('input[name="form[slug]"]').val(make_url($(this).val()));
        });

    if($('input[name="form[slug]"]').length > 0)
        $('input[name="form[name]"]').keyup(function() {
            $('input[name="form[slug]"]').val(make_url($(this).val()));
        });


    $('.tabs li:eq(2) a').tab('show');

    $('a.openModal').click(function() {
        $($(this).attr('href')).modal();
        $('#form_id').val($(this).attr('rel'));
    });

    $('.openTooltip').tooltip();
    $('.popoverShow').popover({
            animation: true,
            html: true,
            placement: 'bottom'
    });
    if($('#form_email').length)
        $('#form_email').keyup(function() {
            $('#form_username').val($(this).val());
        });

    $( ".sortable" ).sortable({
        placeholder: "sortable-destination"
    });
    $( ".sortable" ).disableSelection();

    var spinner = null;
    $('body')
        .ajaxStart(function() {
            spinner = new ajaxLoader(this, {classOveride: 'blue-loader', bgColor: '#000'});
        })
        .ajaxStop(function() {
            spinner.remove();
        });
});