<?php
/**
 * Image path
 */
define("CMS_IMAGE_PATH", __DIR__ . "/images/");
if(!file_exists(CMS_IMAGE_PATH))
    mkdir(CMS_IMAGE_PATH,0777,true);
chmod(CMS_IMAGE_PATH,0777);

if(!file_exists(CMS_IMAGE_PATH."original/"))
    mkdir(CMS_IMAGE_PATH."original/",0777,true);
chmod(CMS_IMAGE_PATH."original/",0777);

/**
 * CMS domain
 */
define("CMS_DOMAIN", "https://82.144.144.73");