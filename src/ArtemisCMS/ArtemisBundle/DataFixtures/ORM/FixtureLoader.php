<?php
namespace ArtemisCMS\ArtemisBundle\DataFixtures\ORM;

use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use ArtemisCMS\ArtemisBundle\Entity\Role;
use ArtemisCMS\ArtemisBundle\Entity\User;
use ArtemisCMS\ArtemisBundle\Entity\Language;
use ArtemisCMS\ArtemisBundle\Entity\AdminMenu;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use ArtemisCMS\ArtemisBundle\Entity\Page;
use ArtemisCMS\ArtemisBundle\Entity\InputType;
use ArtemisCMS\ArtemisBundle\Entity\Config;
use ArtemisCMS\ArtemisBundle\Entity\ImageProfile;

class FixtureLoader implements FixtureInterface, ContainerAwareInterface
{

    private $container;


    private $roles = array();

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->roles = array(
            'ROLE_REGISTRED' => 'Registrovaný uživatel',
            'ROLE_ADMIN' => 'Přístup do administrace',
            'ROLE_ADMIN_PAGE' => 'Práce se stránkama',
            'ROLE_ADMIN_ROLES' => 'Práce s právama',
            'ROLE_ADMIN_USERS' => 'Práce s uživateli',
            'ROLE_ADMIN_SETTINGS' => 'Přístup do nastavení',
            'ROLE_ADMIN_TEMPLATE' => 'Šablony sytému',
            'ROLE_ADMIN_IMAGE_PROFILE' => 'Profily obrázků',
            'ROLE_ADMIN_ARTICLE' => 'Práce s články',
            'ROLE_ADMIN_EXTENSIONS' => 'Práce s dopňky',
            'ROLE_ADMIN_STATICBOX' => 'Práce se statickými boxy',
        );
    }

    public function load(ObjectManager $manager)
    {
        // create the ROLE_ADMIN role
        $roles = array();
        foreach($this->roles as $roleKey => $roleDesc) {
            $role = new Role();
            $role->setName($roleKey);
            $role->setDescription($roleDesc);
            $manager->persist($role);

            $roles[] = $role;
        }

        // create menus
        $this->loadMenu($manager);

        // deafult language
        $defaultLanguage = new Language();
        $defaultLanguage->setKey('cz');
        $defaultLanguage->setName('Čeština');
        $manager->persist($defaultLanguage);

        $ImageProfile = new ImageProfile();
        $ImageProfile->setCrop(true);
        $ImageProfile->setActive(false);
        $ImageProfile->setWidth(120);
        $ImageProfile->setHeight(80);
        $ImageProfile->setName("Administrace obrázek");

        $manager->persist($ImageProfile);

        // create a user
        $user = new User();
        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($user);

        $user->setFirstName('John');
        $user->setSurname('Doe');
        $user->setEmail('pecina.ondrej@gmail.com');
        $user->setUsername('admin');

        // encode and set the password for the user,
        // these settings match our config
        $password = $encoder->encodePassword('admin', $user->getSalt());
        $user->setPassword($password);
        foreach($roles as $userRole)
            $user->getUserRoles()->add($userRole);

        $manager->persist($user);

        // create homepage
        $page = new Page();
        $page->setLanguage($defaultLanguage);
        $page->setSlug('homepage');
        $page->setTitle('Homepage');
        $page->setCreatedBy($user);

        $manager->persist($page);

        $this->loadInputTypes($manager);


        $manager->flush();
    }


    private function loadMenu(ObjectManager $manager) {
        // USERS
        $users = new AdminMenu();
        $users->setName("Uživatelé");
        $users->setAdminRoute('admin_users');
        $users->setRole('ROLE_ADMIN_USERS');
        $users->setPosition(8);
        $manager->persist($users);

            $menu = new AdminMenu();
            $menu->setName("Práva");
            $menu->setParent($users);
            $menu->setAdminRoute('admin_roles');
            $menu->setRole('ROLE_ADMIN_ROLES');
            $manager->persist($menu);

            $menu = new AdminMenu();
            $menu->setName("Facebook uživatelé");
            $menu->setParent($users);
            $menu->setAdminRoute('admin_users_facebook');
            $menu->setRole('ROLE_ADMIN_USERS');
            $manager->persist($menu);

        // Settings
        $settings = new AdminMenu();
        $settings->setName("Nastavení");
        $settings->setAdminRoute('admin_settings');
        $settings->setRole('ROLE_ADMIN');
        $manager->persist($settings);

            $menu2 = new AdminMenu();
            $menu2->setName("Konfigurace");
            $menu2->setParent($settings);
            $menu2->setRole('ROLE_ADMIN_SETTINGS');
            $menu2->setAdminRoute('admin_settings_config');
            $menu2->setPosition(999);
            $manager->persist($menu2);

            $menu2 = new AdminMenu();
            $menu2->setName("Šablony");
            $menu2->setParent($settings);
            $menu2->setRole('ROLE_ADMIN_TEMPLATE');
            $menu2->setAdminRoute('admin_settings_templates');
            $menu2->setPosition(2);
            $manager->persist($menu2);

            $menu2 = new AdminMenu();
            $menu2->setName("Profily obrázků");
            $menu2->setParent($settings);
            $menu2->setRole('ROLE_ADMIN_IMAGE_PROFILE');
            $menu2->setAdminRoute('admin_settings_profiles');
            $menu2->setPosition(2);
            $manager->persist($menu2);

        // SETTINGS END

        $exten = new AdminMenu();
        $exten->setName("Doplňky");
        $exten->setAdminRoute('admin_extensions');
        $exten->setRole('ROLE_ADMIN');
        $exten->setPosition(4);
        $manager->persist($exten);

            $menu2 = new AdminMenu();
            $menu2->setName("Statické boxy");
            $menu2->setParent($exten);
            $menu2->setRole('ROLE_ADMIN_STATICBOX');
            $menu2->setAdminRoute('admin_extensions_staticboxes');
            $menu2->setPosition(1);
            $manager->persist($menu2);


        $menu = new AdminMenu();
        $menu->setName("Stránky");
        $menu->setAdminRoute('admin_page');
        $menu->setRole('ROLE_ADMIN_PAGE');
        $menu->setPosition(0);
        $manager->persist($menu);

        $article = new AdminMenu();
        $article->setName("Články");
        $article->setAdminRoute('admin_article');
        $article->setRole('ROLE_ADMIN_ARTICLE');
        $article->setPosition(1);
        $manager->persist($article);

            $menu2 = new AdminMenu();
            $menu2->setName("Kategorie článků");
            $menu2->setParent($article);
            $menu2->setRole('ROLE_ADMIN_ARTICLE');
            $menu2->setAdminRoute('admin_article_category');
            $menu2->setPosition(1);
            $manager->persist($menu2);

    }




    private function loadInputTypes(ObjectManager $manager) {
        $typeText = new InputType();
        $typeText->setInpuType('text');
        $manager->persist($typeText);

        $textArea = new InputType();
        $textArea->setInpuType('textarea');
        $manager->persist($textArea);

        $typeEmail = new InputType();
        $typeEmail->setInpuType('email');
        $manager->persist($typeEmail);

        $typeInt = new InputType();
        $typeInt->setInpuType('integer');
        $manager->persist($typeInt);

        $ckEditor = new InputType();
        $ckEditor->setInpuType('ckeditor');
        $manager->persist($ckEditor);


        $config = new Config();
        $config->setInpuType($typeText);
        $config->setEditable(true);
        $config->setKeyName('app_facebook_id');
        $config->setDescription('Facebook App ID');
        $config->setConfigGroup(Config::GROUP_OTHER);
        $manager->persist($config);

        $config = new Config();
        $config->setInpuType($typeText);
        $config->setEditable(true);
        $config->setKeyName('app_facebook_secret');
        $config->setDescription('Facebook App Secret');
        $config->setConfigGroup(Config::GROUP_OTHER);
        $manager->persist($config);

        $config = new Config();
        $config->setInpuType($typeText);
        $config->setEditable(true);
        $config->setKeyName('page_name');
        $config->setDescription('Název stránky');
        $config->setHelp('Tento text se automaticky přidá za každou url');
        $config->setConfigGroup(Config::GROUP_SEO);
        $manager->persist($config);

        $config = new Config();
        $config->setInpuType($textArea);
        $config->setEditable(true);
        $config->setKeyName('page_keywords');
        $config->setDescription('Klíčová slova');
        $config->setConfigGroup(Config::GROUP_SEO);
        $manager->persist($config);


        $config = new Config();
        $config->setInpuType($textArea);
        $config->setEditable(true);
        $config->setKeyName('page_description');
        $config->setDescription('Popis stránky');
        $config->setConfigGroup(Config::GROUP_SEO);
        $manager->persist($config);


        $config = new Config();
        $config->setInpuType($ckEditor);
        $config->setEditable(true);
        $config->setKeyName('page_footer');
        $config->setDescription('Patička webu');
        $config->setConfigGroup(Config::GROUP_SEO);
        $manager->persist($config);


        $config = new Config();
        $config->setInpuType($typeEmail);
        $config->setEditable(true);
        $config->setKeyName('contact_email');
        $config->setDescription('Kontaktní email');
        $config->setConfigGroup(Config::GROUP_OTHER);
        $manager->persist($config);


        $config = new Config();
        $config->setInpuType($typeInt);
        $config->setEditable(true);
        $config->setKeyName('items_page_limit');
        $config->setDescription('Počet záznamů na stránku');
        $config->setConfigGroup(Config::GROUP_SHOWSET);
        $manager->persist($config);


    }



}