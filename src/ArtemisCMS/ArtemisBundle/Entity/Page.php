<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\Entity\Repository\Page")
 * @ORM\Table(name="pages", indexes={@ORM\Index(name="slug_index", columns={"slug"})})
 */
class Page
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;


    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\joinColumn(name="user_id", referencedColumnName="id")
     */
    protected $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\joinColumn(name="language_id", referencedColumnName="id")
     */
    protected $language;


    /**
     * @ORM\ManyToMany(targetEntity="PageModule")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $modules;

    /**
     * @ORM\ManyToOne(targetEntity="Page")
     * @var AdminMenu
     */
    protected $parent = null;


    /**
     * @ORM\ManyToOne(targetEntity="Template")
     * @var Template
     */
    protected $template = null;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $position = 0;


    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="external_url")
     */
    protected $externalUrl = null;


    public function __construct()
    {
        $this->modules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\User $createdBy
     */
    public function setCreatedBy(\ArtemisCMS\ArtemisBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * Get createdBy
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set language
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Language $language
     */
    public function setLanguage(\ArtemisCMS\ArtemisBundle\Entity\Language $language)
    {
        $this->language = $language;
    }

    /**
     * Get language
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Add modules
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\PageModule $modules
     */
    public function addPageModule(\ArtemisCMS\ArtemisBundle\Entity\PageModule $modules)
    {
        $this->modules[] = $modules;
    }

    /**
     * Get modules
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Set parent
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Page $parent
     */
    public function setParent(\ArtemisCMS\ArtemisBundle\Entity\Page $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Page 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set position
     *
     * @param smallint $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Get position
     *
     * @return smallint 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set externalUrl
     *
     * @param string $externalUrl
     */
    public function setExternalUrl($externalUrl)
    {
        $this->externalUrl = $externalUrl;
    }

    /**
     * Get externalUrl
     *
     * @return string 
     */
    public function getExternalUrl()
    {
        return $this->externalUrl;
    }

    /**
     * Set template
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Template $template
     */
    public function setTemplate(\ArtemisCMS\ArtemisBundle\Entity\Template $template)
    {
        $this->template = $template;
    }

    /**
     * Get template
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Template 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function __toString() {
        return (string)$this->title;
    }
}