<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="article_category", indexes={@ORM\Index(name="slug_index", columns={"slug"})})
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\CMS\BaseRepository")
 */
class ArticleCategory
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\joinColumn(name="user_id", referencedColumnName="id")
     */
    protected $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\joinColumn(name="language_id", referencedColumnName="id")
     */
    protected $language;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;


    /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\joinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;


    public function __toString() {
        return $this->name;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdBy
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\User $createdBy
     */
    public function setCreatedBy(\ArtemisCMS\ArtemisBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * Get createdBy
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set language
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Language $language
     */
    public function setLanguage(\ArtemisCMS\ArtemisBundle\Entity\Language $language)
    {
        $this->language = $language;
    }

    /**
     * Get language
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set image
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Image $image
     */
    public function setImage(\ArtemisCMS\ArtemisBundle\Entity\Image $image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }
}