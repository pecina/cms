<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\CMS\BaseRepository")
 * @ORM\Table(name="image_profiles", indexes={@ORM\Index(name="active_index", columns={"active"})})
 */
class ImageProfile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length="255", name="name")
     *
     * @var string $name
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     *
     * @var string $width
     */
    protected $width;

    /**
     * @ORM\Column(type="integer")
     *
     * @var string $height
     */
    protected $height;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean $active
     */
    protected $active = true;


    /**
     * @ORM\Column(type="boolean")
     *
     * @var string $crop
     */
    protected $crop = false;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set width
     *
     * @param integer $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set crop
     *
     * @param boolean $crop
     */
    public function setCrop($crop)
    {
        $this->crop = $crop;
    }

    /**
     * Get crop
     *
     * @return boolean 
     */
    public function getCrop()
    {
        return $this->crop;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
}