<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="articles", indexes={@ORM\Index(name="slug_index", columns={"slug"}, @ORM\Index(name="order_index", columns={"title, vote"}))})
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\Entity\Repository\Article")
 */
class Article
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\joinColumn(name="user_id", referencedColumnName="id")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\joinColumn(name="language_id", referencedColumnName="id")
     */
    protected $language;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $perex;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\joinColumn(name="master_image_id", referencedColumnName="id")
     */
    protected $masterImage;

    /**
     * @ORM\ManyToMany(targetEntity="Image")
     */
    protected $images;

    /**
     * @ORM\ManyToMany(targetEntity="ArticleCategory")
     */
    protected $categories;


    /**
     * @ORM\Column(type="boolean")
     */
    protected $published;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $vote = 0;

    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set perex
     *
     * @param text $perex
     */
    public function setPerex($perex)
    {
        $this->perex = $perex;
    }

    /**
     * Get perex
     *
     * @return text 
     */
    public function getPerex()
    {
        return $this->perex;
    }


    /**
     * Set createdBy
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\User $createdBy
     */
    public function setCreatedBy(\ArtemisCMS\ArtemisBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * Get createdBy
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set language
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Language $language
     */
    public function setLanguage(\ArtemisCMS\ArtemisBundle\Entity\Language $language)
    {
        $this->language = $language;
    }

    /**
     * Get language
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set masterImage
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Image $masterImage
     */
    public function setMasterImage(\ArtemisCMS\ArtemisBundle\Entity\Image $masterImage)
    {
        $this->masterImage = $masterImage;
    }

    /**
     * Get masterImage
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\Image 
     */
    public function getMasterImage()
    {
        return $this->masterImage;
    }

    /**
     * Add images
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\Image $images
     */
    public function addImage(\ArtemisCMS\ArtemisBundle\Entity\Image $images)
    {
        $this->images[] = $images;
    }

    /**
     * Get images
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set published
     *
     * @param boolean $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set content
     *
     * @param text $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return text 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add categories
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\ArticleCategory $categories
     */
    public function addArticleCategory(\ArtemisCMS\ArtemisBundle\Entity\ArticleCategory $categories)
    {
        $this->categories[] = $categories;
    }


    /**
     * Add categories
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\ArticleCategory $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * Get categories
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set vote
     *
     * @param integer $vote
     */
    public function setVote($vote)
    {
        $this->vote = $vote;
    }

    /**
     * Get vote
     *
     * @return integer 
     */
    public function getVote()
    {
        return ($this->vote?$this->vote:0);
    }
}