<?php
namespace ArtemisCMS\ArtemisBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ArtemisCMS\ArtemisBundle\CMS\BaseRepository;

/**
 * Admin page repository
 * For simple using menu
 * @author Pecina Ondřej
 * @version 1.0
 */
class Page extends BaseRepository  {


    /**
     * Create pages tree
     * @param null $parentId
     * @return array
     */
    public function createTree($parentId = null) {
        $pages = array();
        $main = array();
        if($parentId === null)
            $main = $this->getEntityManager()
                ->createQuery("SELECT p FROM ArtemisBundle:Page p WHERE p.parent IS NULL ORDER BY p.position ASC")
                ->getResult();
        else
            $main = $this->getEntityManager()
                ->createQuery("SELECT p FROM ArtemisBundle:Page p WHERE p.parent = $parentId ORDER BY p.position ASC")
                ->getResult();
        // find childs
        if($main && count($main) > 0) {
            foreach( $main as $item ) {
                $pages[] = array(
                    'item'     => $item,
                    'childs'   => $this->createTree($item->getId())
                );
            }
        }
        return $pages;
    }
}