<?php
namespace ArtemisCMS\ArtemisBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use ArtemisCMS\ArtemisBundle\CMS\BaseRepository;

/**
 * User repository
 * For simple using admin work
 * @author Pecina Ondřej
 * @version 1.0
 */
class User extends BaseRepository implements \Symfony\Component\Security\Core\User\UserProviderInterface {

    /**
     * Fin user by user name
     * @param $username
     * @return \ArtemisCMS\ArtemisBundle\Entity\User
     */
    public function loadUserByUsername($username)
    {
        return $this->findOneBy(array('login' => $username));
    }

    /**
     * Load user
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \ArtemisCMS\ArtemisBundle\Entity\User
     */
    public function loadUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param AccountInterface $account
     * @return \ArtemisCMS\ArtemisBundle\Entity\User
     */
    public function loadUserByAccount(AccountInterface $account)
    {
        return $this->loadUserByUsername($account->getUsername());
    }


    /**
     * Find only facebook users
     * @return array
     */
    public function findOnlyFacebookUsers() {
        return $this->getEntityManager()
            ->createQuery("SELECT p FROM ArtemisBundle:User p WHERE p.facebook IS NOT NULL ORDER BY p.id DESC")
            ->getResult();
    }

    /**
     * @param $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'ArtemisCMS\ArtemisBundle\Entity\User';
    }

    /**
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \ArtemisCMS\ArtemisBundle\Entity\User
     */
    public function refreshUser(UserInterface $user) {
        return $this->loadUserByUsername($user->getUsername());
    }


    /**
     * Find all items whit limi adn offset facebook
     * @param $limit
     * @param $offSet
     * @param null $orderBy
     * @param null $orderType
     * @return array
     */
    public function findAllItemsFb($limit, $offSet, $orderBy = null, $orderType = null) {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('a'));
        $qb->add('orderBy', "a.$orderBy $orderType");
        $qb->andWhere('a.facebook IS NOT NULL');
        $query = $qb->getQuery();

        if($limit && $limit > 0) {
            $query->setMaxResults($limit);
            $query->setFirstResult($offSet);
        }

        return $query->getResult();
    }


    /**
     * Find all items count facebook
     * @return int
     */
    public function findAllItemsCountFb() {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('count(a.id) as cnt'));
        $qb->andWhere('a.facebook IS NOT NULL');
        $query = $qb->getQuery();

        $result = $query->getResult();
        return $result[0]['cnt'];
    }
}