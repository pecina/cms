<?php
namespace ArtemisCMS\ArtemisBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ArtemisCMS\ArtemisBundle\CMS\BaseRepository;

/**
 * Article repository
 * @author Pecina Ondřej
 * @version 1.0
 */
class Article extends BaseRepository  {

    /**
     * Find articles for module on page
     * @param array $categories
     * @param $orderBy
     * @param $orderType
     * @param $limit
     * @param int $offSet
     * @return array
     */
    public function findModule(array $categories, $orderBy, $orderType, $limit, $offSet = 0) {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('a'))
            ->leftJoin('a.categories', 'c');
        $qb->add('where', $qb->expr()->in('c.id', $categories));
        $qb->andWhere('a.published = 1');
        $qb->add('orderBy', "a.$orderBy $orderType");
        $qb->add('groupBy', "a.id");
        $query = $qb->getQuery();

        if($limit && $limit > 0) {
            $query->setMaxResults($limit);
            $query->setFirstResult($offSet);
        }
        return $query->getResult();
    }

    /**
     * Article slide article
     * @param array $categories
     * @param $orderBy
     * @param $orderType
     * @param int $offSet
     * @return Article
     */
    public function findOneSlide(array $categories, $orderBy, $orderType, $offSet = 0) {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('a'))
            ->leftJoin('a.categories', 'c');
        $qb->add('where', $qb->expr()->in('c.id', $categories));
        $qb->andWhere('a.published = 1');
        $qb->add('orderBy', "a.$orderBy $orderType");
        $query = $qb->getQuery();
        $query->setFirstResult($offSet);
        $query->setMaxResults(1);
        $result = $query->getResult();
        return $result[0];
    }





    /**
     * Article count
     * @param array $categories
     * @param $orderBy
     * @param $orderType
     * @return int - count of articles
     */
    public function countOfArticles(array $categories, $orderBy, $orderType) {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('count(a) as cnt'))
            //->from($this->_entityName, 'a')
            ->leftJoin('a.categories', 'c');
        $qb->add('where', $qb->expr()->in('c.id', $categories));
        $qb->andWhere('a.published = 1');
        $qb->add('orderBy', "a.$orderBy $orderType");
        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result[0]['cnt'];
    }



}