<?php
namespace ArtemisCMS\ArtemisBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ArtemisCMS\ArtemisBundle\CMS\BaseRepository;

/**
 * Admin menu repository
 * For simple using menu
 * @author Pecina Ondřej
 * @version 1.0
 */
class AdminMenu extends BaseRepository  {


    /**
     * Create admin menu tree
     * @param null $parentId
     * @return array
     */
    public function createTree($parentId = null) {
        $menus = array();
        $main = new ArrayCollection();
        if($parentId === null)
            $main = $this->getEntityManager()
                ->createQuery("SELECT p FROM ArtemisBundle:AdminMenu p WHERE p.parent IS NULL ORDER BY p.position ASC")
                ->getResult();
        else
            $main = $this->getEntityManager()
                ->createQuery("SELECT p FROM ArtemisBundle:AdminMenu p WHERE p.parent = $parentId ORDER BY p.position ASC")
                ->getResult();
        // find childs
        if($main && count($main) > 0) {
            foreach( $main as $item ) {
                $menus[] = array(
                    'menu_item'     => $item,
                    'menu_childs'   => $this->createTree($item->getId())
                );
            }
        }

        return $menus;
    }
}