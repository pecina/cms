<?php
namespace ArtemisCMS\ArtemisBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ArtemisCMS\ArtemisBundle\CMS\BaseRepository;

/**
 * Config repository
 * @author Pecina Ondřej
 * @version 1.0
 */
class Config extends BaseRepository  {


    /**
     * Find all result and make array indexed by keyName
     * @return array
     */
    public function findAllKey() {
        $rows = array();
        $configs = $this->findAll();
        foreach($configs as $config) {
            $rows[$config->getKeyName()] = $config;
        }
        return $rows;
    }

    /**
     * Find all by the group - only editable
     * @param $group
     * @return array
     */
    public function findEditable($group) {
        return $this->findBy(array(
            'editable' => true,
            'configGroup' => $group
        ));
    }

    /**
     * Return entity by key
     * @param $key
     * @return object
     */
    public function findByKey($key) {
        return $this->findOneBy(array('keyName' => $key));
    }
}