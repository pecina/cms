<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="template")
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\CMS\BaseRepository")
 */
class Template
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length="255", name="key_name")
     *
     * @var string $name
     */
    protected $name;


    /**
     * @ORM\Column( nullable="true", columnDefinition="LONGTEXT")
     *
     * @var string $content
     */
    protected $content;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return TEXT 
     */
    public function getContent()
    {
        return $this->content;
    }

    public function __toString() {
        return $this->getName();
    }
}