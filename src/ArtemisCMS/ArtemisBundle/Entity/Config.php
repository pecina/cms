<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\Entity\Repository\Config")
 * @ORM\Table(name="config", indexes={@ORM\Index(name="key_index", columns={"key_name"})})
 */
class Config
{
    const GROUP_SEO     = 'seo';
    const GROUP_OTHER   =  'other';
    const GROUP_SHOWSET   =  'showset';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length="255", name="key_name", unique=true)
     *
     * @var string $name
     */
    protected $keyName;


    /**
     * @ORM\Column(type="string", length="500", nullable="true")
     *
     * @var string $description
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="InputType")
     * @var InputType
     */
    protected $inpuType = null;

    /**
     * @ORM\Column(type="text", columnDefinition="LONGTEXT", nullable=true)
     */
    protected $value;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $editable = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $help;

    /**
     * @ORM\Column(type="string", length="20", nullable=false, name="config_group")
     */
    protected $configGroup;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyName
     *
     * @param string $keyName
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;
    }

    /**
     * Get keyName
     *
     * @return string 
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set value
     *
     * @param text $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return text 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set inpuType
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\InputType $inpuType
     */
    public function setInpuType(\ArtemisCMS\ArtemisBundle\Entity\InputType $inpuType)
    {
        $this->inpuType = $inpuType;
    }

    /**
     * Get inpuType
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\InputType 
     */
    public function getInpuType()
    {
        return $this->inpuType;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
    }

    /**
     * Get editable
     *
     * @return boolean 
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set help
     *
     * @param string $help
     */
    public function setHelp($help)
    {
        $this->help = $help;
    }

    /**
     * Get help
     *
     * @return TEXT 
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * Set configGroup
     *
     * @param string $configGroup
     */
    public function setConfigGroup($configGroup)
    {
        switch($configGroup) {
            case self::GROUP_OTHER:
            case self::GROUP_SEO:
                $this->configGroup = $configGroup;
                break;
            default:
                throw new \Exception('Neznámý typ!');
                break;
        }
    }

    /**
     * Get configGroup
     *
     * @return string 
     */
    public function getConfigGroup()
    {
        return $this->configGroup;
    }
}