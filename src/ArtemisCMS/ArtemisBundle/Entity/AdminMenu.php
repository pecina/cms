<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="admin_menu")
 * @ORM\Entity(repositoryClass="ArtemisCMS\ArtemisBundle\Entity\Repository\AdminMenu")
 */
class AdminMenu
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length="255")
     *
     * @var string $name
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length="80", nullable=true)
     *
     * @var string $role
     */
    protected $role;

    /**
     * @ORM\Column(type="string", length="80", name="admin_route")
     *
     * @var string $admin_route
     */
    protected $adminRoute;

    /**
     * @ORM\ManyToOne(targetEntity="AdminMenu")
     * @var AdminMenu
     */
    protected $parent = null;

    /**
     * @ORM\Column(type="integer")
     */
    protected $position = 99;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set adminRoute
     *
     * @param string $adminRoute
     */
    public function setAdminRoute($adminRoute)
    {
        $this->adminRoute = $adminRoute;
    }

    /**
     * Get adminRoute
     *
     * @return string 
     */
    public function getAdminRoute()
    {
        return $this->adminRoute;
    }


    /**
     * Set parent
     *
     * @param ArtemisCMS\ArtemisBundle\Entity\AdminMenu $parent
     */
    public function setParent(\ArtemisCMS\ArtemisBundle\Entity\AdminMenu $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return ArtemisCMS\ArtemisBundle\Entity\AdminMenu 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set position
     *
     * @param integer $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set role
     *
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }
}