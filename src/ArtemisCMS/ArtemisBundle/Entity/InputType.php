<?php
namespace ArtemisCMS\ArtemisBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="input_types")
 */
class InputType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length="255", name="input_type")
     *
     * @var string $name
     */
    protected $inpuType;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inpuType
     *
     * @param string $inpuType
     */
    public function setInpuType($inpuType)
    {
        $this->inpuType = $inpuType;
    }

    /**
     * Get inpuType
     *
     * @return string 
     */
    public function getInpuType()
    {
        return $this->inpuType;
    }
}