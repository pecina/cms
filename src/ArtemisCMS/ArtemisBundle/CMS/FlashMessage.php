<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

/**
 * Object for display flash messages
 * @author Pecina Ondrej
 * @version 1.0
 */
class FlashMessage {

    // message types
    const TYPE_INFO = "info";
    const TYPE_ERR = "error";
    const TYPE_SUCCESS = "success";

    /**
     * Message
     * @var string
     */
    private $message;

    /**
     * Message type
     * @var string enum 'err', 'info', 'warning'
     */
    private $type;


    public function __construct($message, $type = self::TYPE_INFO) {
        $this->message = $message;
        $this->type = $type;
    }

    /**
     * Return messge
     * @return string - message
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Return message type, possible returning values:
     * <br>
     * 'info' message is only info
     * <br>
     * 'error' message is error
     * <br>
     * 'success' - message is success
     * <br>
     * @return string
     */
    public function getType() {
        return $this->type;
    }
}