<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Acl\Exception\Exception;
use ArtemisCMS\ArtemisBundle\CMS\Facebook;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
/**
 * Base controller
 * @author Pecina Ondřej <pecina.ondrej@gmail.com>
 * @version 1.0
 */
abstract class BaseController extends Controller
{

    /**
     * Load global config values
     * @return mixed
     */
    public function loadConfig() {
        // create configs if not extis
        $values = unserialize($this->getSessionValue('configvalues'));
        if(!$values) {
            $values = $this->getRepository('Config')->findAllKey();
            $this->setSessionValue('configvalues', serialize($values));
        }
        return $values;
    }

    /**
     * Login user manually
     * @param \ArtemisCMS\ArtemisBundle\Entity\User $User
     */
    public function loginUser(\ArtemisCMS\ArtemisBundle\Entity\User $User) {
        $token = new UsernamePasswordToken($User, null, 'main', $User->getRoles());
        $this->get('security.context')->setToken($token);
    }

    /**
     * Clear global config values
     */
    public function clearConfig() {
        $this->setSessionValue('configvalues', null);
    }

    /**
     * Return config value
     * @param $key
     * @return null
     */
    public function getConfigValue($key) {
        $values = $this->loadConfig();
        return (isset($values[$key])?$values[$key]->getValue():null);
    }

    /**
     * Display flash message
     * @param $message
     * @param string $type
     */
    public function flashMessage($message, $type = FlashMessage::TYPE_INFO) {
        $messages = unserialize($this->getSessionValue('flash_message'));
        if(!$messages)
            $messages = array();
        $messages[] = new FlashMessage($message, $type);
        $this->setSessionValue('flash_message', serialize($messages));
    }

    /**
     * Return list of messages
     * @return array
     */
    public function getFlashMessages() {

        $messages = unserialize($this->getSessionValue('flash_message'));
        $this->setSessionValue('flash_message', serialize(array()));
        return $messages;
    }

    /**
     * Get session value
     * @param $key
     * @return mixed|null
     */
    public function getSessionValue($key) {
        $session = $this->getRequest()->getSession();
        return $session->get($key);
    }

    /**
     * Set value into session
     * @param $key - key
     * @param $value - value
     */
    public function setSessionValue($key, $value) {
        $session = $this->getRequest()->getSession();
        $session->set($key,$value);
        return $value;
    }

    /**
     * Set sesison values
     * @param array $values  - $key => $value
     */
    public function setSessionValues(array $values){
        $session = $this->getRequest()->getSession();
        foreach($values as $key => $value)
            $session->set($key,$value);
    }

    /**
     * Return $request get param
     * @param $key
     * @return mixed|null
     */
    public function requestGetParam($key) {
        $request = $this->getRequest();
        return $request->get($key);
    }

    /**
     * Returns params array <br>
     * Call $this->requestGetParams($parName1,$parName2,......$parName9999);
     * @return array - list of params
     */
    public function requestGetParams() {
        // retur params
        $params = array();
        // parse request
        $request = $this->getRequest();
        $args = func_get_args();
        $countParams = func_num_args();
        for($i = 0; $i < $countParams; $i++) {
            $key = $args[$i];
            $params[$key] = $request->get($key);
        }
        return $params;
    }

    /**
     * Get entity manager
     * @return \Symfony\Bundle\DoctrineBundle\EntityManager
     */
    public function getEm() {
        return $this->getDoctrine()
            ->getEntityManager();
    }

    /**
     * Returns entity repository
     * @param $entityName
     * @return mixed
     */
    public function getRepository($entityName) {
        return $this->getDoctrine()
            ->getEntityManager()
            ->getRepository('ArtemisBundle:' . $entityName);
    }


    /**
     * Translate text
     * @param $text
     * @return mixed
     */
    public function translate($text) {
        return $this->get('translator')->trans($text);
    }


    /**
     * Render to template front modules
     * <br>
     * This method auto-detect render hash
     * <br>
     * @param $fileName - template file name without extension
     * @param array $args - template arguments
     * @param string $extension - extension of template, default: .twig
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderFront($fileName, array $args = array(), $extension = ".html.twig")
    {
        if(!isset($args['page']))
            $args['page'] = "";
        $args['menu'] = $this->getRepository('Page')->createTree();
        return $this->myRender($fileName,'Front', $args, $extension);
    }


    /**
     * Render user defined template
     * <br>
     * This method auto-detect render hash
     * <br>
     * @param $fileName - template file name without extension
     * @param array $args - template arguments
     * @param string $extension - extension of template, default: .twig
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderTemplate($fileName, array $args = array(), $extension = ".html.twig")
    {
        return $this->myRender($fileName,'Template', $args, $extension, 'Front');
    }


    /**
     * Render to template module
     * <br>
     * This method auto-detect render hash
     * <br>
     * @param $fileName - template file name without extension
     * @param array $args - template arguments
     * @param string $extension - extension of template, default: .twig
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderModule($fileName, array $args = array(), $extension = ".html.twig")
    {
        return $this->myRender($fileName,'Module', $args, $extension, 'Admin');
    }

    /**
     * Render to template admin
     * <br>
     * This method auto-detect render hash
     * <br>
     * @param $fileName - template file name without extension
     * @param array $args - template arguments
     * @param string $extension - extension of template, default: .twig
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderAdmin($fileName, array $args = array(), $extension = ".html.twig")
    {
        $args['languages'] = Languages::$list;
        $args['menu'] = $this->getMenuTree();
        return $this->myRender($fileName, 'Admin', $args, $extension);
    }

    /**
     * Get menu tree
     * @return mixed
     */
    private function getMenuTree() {
        $tree = unserialize($this->getSessionValue('admin_menu'));
        if(!$tree) {
            $tree = $this->getRepository('AdminMenu')->createTree();
            $this->setSessionValue('admin_menu', serialize($tree));
        }
        return $tree;
    }

    /**
     * Render to template
     * <br>
     * This method auto-detect render hash
     * <br>
     * @param $fileName - template file name without extension
     * @param array $args - template arguments
     * @param string $extension - extension of template, default: .twig
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function myRender($fileName, $dir, array $args = array(), $extension = ".html.twig", $replace = null)
    {
        // load config
        $this->loadConfig();
        if(!$replace)
            $replace = $dir;
        // get class name with NS
        $class = get_class($this);
        // optimize right
        $class = str_replace("ArtemisCMS\\", "", str_replace("Controller", "", str_replace("\\Controller", "", $class)));
        $class = str_replace($replace.":",$dir."\\",ucfirst(str_replace("\\", ":", $class)));
        // add login to template
        $args['messages'] = $this->getFlashMessages();
        return $this->render($class . ":" . $fileName . $extension, $args);
    }


    /**
     * Get facebook API
     * @return \ArtemisCMS\ArtemisBundle\CMS\Facebook
     */
    public function getFacebook() {
        $facebook = $this->get('facebook_api');
        if($this->getConfigValue('app_facebook_id') == null)
            return null;
        $facebook->load($this->getConfigValue('app_facebook_id'), $this->getConfigValue('app_facebook_secret'));
        return $facebook;
    }
}