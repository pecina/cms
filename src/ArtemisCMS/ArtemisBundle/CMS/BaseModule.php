<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

/**
 * Abstrac class for definition modules
 * @author Pecina Ondřej
 * @version 1.0
 */
abstract class BaseModule extends BaseAdminController {

    /**
     * Function for render settings form in admin
     * @abstract
     * @param $page_id
     */
    abstract function pageAction($page_id, $id);

    /**
     * Function for render frontend detail
     * @abstract
     * @param $page_id
     */
    abstract function frontendAction($page_id, $id);


    /**
     * Have to returns string module name
     * @abstract
     */
    abstract function getModuleName();

    /**
     * Return module Settings
     * @param $id
     * @return array
     */
    protected function getSettings($id) {
        return unserialize($this->getRepository('PageModule')
            ->find($id)->getSettings());
    }
}

