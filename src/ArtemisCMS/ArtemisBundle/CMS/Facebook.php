<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

/**
 * Facebook class service
 * @author Pecina Ondřej
 * @version 1.0
 */
class Facebook
{
    // INFORMATION RIGHTS
    const RIGHT_USER_EMAIL  = 'email';
    const RIGHT_USER_DETAIL = 'user_about_me';
    const RIGHT_USER_PHOTO  = 'user_photos';

    // WORK RIGHTS
    const RIGHT_DO_PUHBLISH = 'publish_stream';

    /**
     * Facebook api connector
     * @var \Facebook
     */
    private $facebook;

    /**
     * @var
     */
    private $user;

    private $app_id;
    private $app_secret;

    /**
     * Facebook class service
     */
    public function __construct() {}

    /**
     * Load facebook api
     * @param $app_id
     * @param $app_secret
     */
    public function load($app_id, $app_secret)
    {
        $this->app_id = $app_id;
        $this->app_secret = $app_secret;
        $this->facebook = new \Facebook(array(
            'appId' => $app_id,
            'secret' => $app_secret,
        ));
        if(!$this->facebook)
            throw new \Exception('Cant create facebook API!');
    }

    /**
     * Returns facebook
     * @return \Facebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Return facebook user
     * @return mixed
     */
    public function getUser() {
        if(!$this->user) {
            $this->user = $this->facebook->getUser();
        }
        return $this->user;
    }

    /**
     * Check facebook status login
     * @return bool
     */
    public function isLoged()
    {

        $user = $this->facebook->getUser();

        if ($user) {
            try {
                $user_profile = $this->facebook->api('/me');
                $this->getUserInfo();
            } catch (FacebookApiException $e) {
                $user = NULL;
            }
            catch (Exception $e) {
                $user = NULL;
            }
        }
        if(!$user) {
            $this->user = null;
            return false;
        }
        else {
            $this->user = $user;
            return true;
        }
    }

    /**
     * Get login url
     * @param array $scopes
     * @param $url
     */
    public function getLoginUrl($scopes, $url) {
        return $this->facebook->getLoginUrl(array(
            'scope'		    => $scopes, // Permissions to request from the user
            'redirect_uri'	=> $url, // URL to redirect the user to once the login/authorization process is complete.
        ));
    }

    /**
     * GetLogin url
     * @param $url - redirect after logout
     * @return mixed
     */
    public function getLogoutUrl($redirectUrl) {
        $url = $this->facebook->getLogoutUrl();
        $this->load($this->app_id, $this->app_secret);
        return $url;
    }

    /**
     * Publish stream to user wall
     * @param $message
     * @param $name
     * @param $caption
     * @param string $description
     * @param string $link
     * @param string $picture
     */
    public function publishStream($message, $name, $caption, $description = "", $link = "", $picture = "" ) {
        $data = array(
            'message'   => $message,
            'name'      => $name,
            'caption'   => $caption
        );
        if($description)
            $data['description'] = $description;
        if($picture)
            $data['picture'] = $picture;
        if($link)
            $data['link'] = $link;
        try{
            $publishStream = $this->facebook->api("/" . $this->getUser() . "/feed", 'post', $data);
        }catch(FacebookApiException $e){
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Returns loged user info
     * @return array
     */
    public function getUserInfo() {
        $query = array(
            array('method' => 'GET', 'relative_url' => '/'.$this->getUser())
        );
        try{
            $batchResponse = $this->facebook->api('?batch='.json_encode($query), 'POST');
        }catch(Exception $o){
            error_log($o);
        }
        $user_info		= json_decode($batchResponse[0]['body'], TRUE);
        return $user_info;
    }

    /**
     * Returns user facebook id
     * @return mixed
     */
    public function getUserId() {
        $userData = $this->getUserInfo();
        return $userData['id'];
    }


    public function getUserPhotoLink() {
        return "https://graph.facebook.com/" . $this->getUserId() . "/picture?type=large";
    }
}