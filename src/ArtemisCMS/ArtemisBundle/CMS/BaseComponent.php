<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

/**
 * Abstract class for component
 * @author Pecina Ondřej
 * @version 1.0
 */
abstract class BaseComponent extends BaseController {
    abstract function renderAction();
}

