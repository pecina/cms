<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Base repository
 * @author Pecina Ondřej
 * @version 1.0
 */
class BaseRepository extends EntityRepository  {


    /**
     * Find all results
     * @return array
     */
    public function findAll() {
        return $this->getEntityManager()
            ->createQuery("SELECT p FROM " . $this->_entityName . " p ORDER BY p.id DESC")
            ->getResult();
    }

    /**
     * Find item by slug
     * @param $slug
     * @return object
     */
    public function findBySlug($slug) {
        return $this->findOneBy(array('slug' => $slug));
    }


    /**
     * Find item by slug
     * @param $slug
     * @return object
     */
    public function findActive() {
        return $this->findBy(array('active' => true));
    }


    /**
     * Find all items whit limi adn offset
     * @param $limit
     * @param $offSet
     * @param null $orderBy
     * @param null $orderType
     * @return array
     */
    public function findAllItems($limit, $offSet, $orderBy = null, $orderType = null) {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('a'));
        $qb->add('orderBy', "a.$orderBy $orderType");
        $query = $qb->getQuery();

        if($limit && $limit > 0) {
            $query->setMaxResults($limit);
            $query->setFirstResult($offSet);
        }

        return $query->getResult();
    }


    /**
     * Find all items count
     * @return int
     */
    public function findAllItemsCount() {
        $qb = $this->createQueryBuilder('a');
        $qb->select(array('count(a.id) as cnt'));
        $query = $qb->getQuery();

        $result = $query->getResult();
        return $result[0]['cnt'];
    }
}