<?php

namespace ArtemisCMS\ArtemisBundle\CMS;

final class Languages {

    /**
     * Static only
     */
    private function __construct(){}

    public static $list = array(
        'cz' => 'Čeština',
        //'en' => 'English',
        //'sk' => 'Slovak'
    );
}