<?php
namespace ArtemisCMS\ArtemisBundle\CMS;


final class Image
{

    public static  function storeImage($imageFile, $path) {

    }


    /**
     * need GD library (first PHP line WIN: dl("php_gd.dll"); UNIX: dl("gd.so");
     * www.boutell.com/gd/
     * interval.cz/clanky/php-skript-pro-generovani-galerie-obrazku-2/
     * cz.php.net/imagecopyresampled
     * www.linuxsoft.cz/sw_detail.php?id_item=871
     * www.webtip.cz/art/wt_tech_php/liquid_ir.html
     * php.vrana.cz/zmensovani-obrazku.php
     * diskuse.jakpsatweb.cz/
     *
     * @param string $file_in Vstupni soubor (mel by existovat)
     * @param string $file_out Vystupni soubor, null ho jenom zobrazi (taky kdyz nema pravo se zapsat :)
     * @param int $width Vysledna sirka (maximalni)
     * @param int $height Vysledna vyska (maximalni)
     * @param bool $crop Orez (true, obrazek bude presne tak velky), jinak jenom Resample (udane maximalni rozmery)
     * @param int $type_out IMAGETYPE_type vystupniho obrazku
     * @return bool Chyba kdyz vrati false
     */
    public static function resizeImage($file_in, $file_out = null, $width = null, $height = null, $crop = null, $type_out = null)
    {
        if ($file_in !== "" && File_Exists($file_in)) {
            // get width, height, IMAGETYPE_type
            list($w, $h, $type) = GetImageSize($file_in); //array of width, height, IMAGETYPE, "height=x width=x" (string)
            // print("Input: ". image_type_to_mime_type($type) ." $w x $h<br>");

            // vyber funkce pro nacteni vstupu
            switch ($type)
            {
                case IMAGETYPE_JPEG:
                    $imgIn = 'ImageCreateFromJPEG';
                    break;
                case IMAGETYPE_PNG :
                    $imgIn = 'ImageCreateFromPNG';
                    break;
                case IMAGETYPE_GIF :
                    $imgIn = 'ImageCreateFromGIF';
                    break;
                case IMAGETYPE_WBMP:
                    $imgIn = 'ImageCreateFromWBMP';
                    break;
                default:
                    return false;
                    break;
            }

            // jaky typ chcem vyrobit? jestli je to fuk, tak ten samy...
            if ($type_out == null) { // ...krom Wbitmapy, ta je blba :)
                $type_out = ($type == IMAGETYPE_WBMP) ? IMAGETYPE_PNG : $type;
            }
            switch ($type_out)
            {
                case IMAGETYPE_JPEG:
                    $imgOut = 'ImageJPEG';
                    break;
                case IMAGETYPE_PNG :
                    $imgOut = 'ImagePNG';
                    break;
                case IMAGETYPE_GIF :
                    $imgOut = 'ImageGIF';
                    break;
                case IMAGETYPE_WBMP:
                    $imgOut = 'ImageWBMP';
                    break; // bitmapa je blbost
                default:
                    return false;
                    break;
            }

            // toz, jak ma byt vysledek veliky?
            if ($width == null || $width == 0) { // neni zadana sirka
                $width = $w;
            }
            else if ($height == null || $height == 0) { // neni zadana vyska, ale sirka jo... hmm, toz je dam stejne :)
                $height = $width;
            }

            if ($height == null || $height == 0) { // neni zadana vyska, ale sirka taky ne :)
                $height = $h;
            }

            // jestli se ma jenom zmensovat...
            if (!$crop) { // prepocti velikost - nw, nh (new width/height)
                $scale = (($width / $w) < ($height / $h)) ? ($width / $w) : ($height / $h); // vyber mensi pomer

                $src = array(0, 0, $w, $h);
                $dst = array(0, 0, floor($w * $scale), floor($h * $scale));
            }
            else { // bude se orezavat
                $scale = (($width / $w) > ($height / $h)) ? ($width / $w) : ($height / $h); // vyber vetsi pomer a zkus to nejak dopasovat...
                $newW = $width / $scale; // jak by mel byt zdroj velky (pro poradek :)
                $newH = $height / $scale;

                // ktera strana precuhuje vic (kvuli chybe v zaokrouhleni)
                if (($w - $newW) > ($h - $newH)) {
                    $src = array(floor(($w - $newW) / 2), 0, floor($newW), $h);
                }
                else {
                    $src = array(0, floor(($h - $newH) / 2), $w, floor($newH));
                }

                $dst = array(0, 0, floor($width), floor($height));
            }

            // print("posX\t posY\t widt\t heig\t - transformation:<br>"."$src[0]\t $src[1]\t $src[2]\t $src[3]\t <br>"."$dst[0]\t $dst[1]\t $dst[2]\t $dst[3]\t <br>");
            // print("Output: ". image_type_to_mime_type($type) ." $dst[2] x $dst[3]<br>");

            // proved resampling...
            if (@$image1 = $imgIn($file_in)) {
                $image2 = ImageCreateTruecolor($dst[2], $dst[3]);
                ImageCopyResampled($image2, $image1, $dst[0], $dst[1], $src[0], $src[1], $dst[2], $dst[3], $src[2], $src[3]);

                // pokud se ma pouze zobrazit, dej vedet co prijde (MIME type)... (tedko uz se snad nic nepokazi)
                if ($file_out == null) {
                    header("Content-type: " . image_type_to_mime_type($type_out));
                }

                // set jpeg/png quality to 85 before saving
                if ($type_out == IMAGETYPE_JPEG || $type_out == IMAGETYPE_PNG) {

                    $imgOut($image2, $file_out, 9);
                }
                else { // save the image
                    $imgOut($image2, $file_out);
                }

                ImageDestroy($image1); // free memory
                ImageDestroy($image2);

                return true; // tohle je jediny misto, kde se da vratit uspech
            }
        }
        return false;
    }
}
