<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

use Symfony\Component\Security\Acl\Exception\Exception;
/**
 * Base Admin(Backend) controller
 * @author Pecina Ondřej <pecina.ondrej@gmail.com>
 * @version 1.0
 */
abstract class BaseAdminController extends BaseController
{

    /**
     * Get loged user object
     * @return ArtemisCMS\ArtemisBundle\Entity\User
     */
    public function getLoggedUser() {
        return $this->getRepository('User')
            ->find($this->container->get('security.context')->getToken()->getUser()->getId());
    }

    /**
     * Load existing modules
     * @param $dir __DIR__
     * @param $ns __NS__
     * @return array
     */
    public function getModules($dir, $ns) {
       // $modules = $this->getSessionValue('modules');
        $modules = array();
        if($modules)
            return unserialize($modules);
        else {
            $classes = array();
            $handle = opendir($dir);
            while (false !== ($entry = readdir($handle))) {
                $classes[] = str_replace(".php","",$entry);
            }
            $classes = array_filter($classes, function ($class) {
                return strpos($class, 'Controller') !== FALSE;
            });
            foreach($classes as &$class) {
                $class=$ns."\\".$class;
            }
            // check module
            $classes = array_filter($classes, function ($class) {
                $classIns = new $class;
                if($classIns instanceof BaseModule)
                    return true;
                else return false;
            });
            //print_r($classes);
            $retVal = array();
            $classes = array_values($classes);
            foreach($classes as $className) {
                $instance = new $className;
                $route = str_replace("ArtemisCMS\\","",str_replace("Controller","",str_replace("\\Controller\\",":",$className)));
                $retVal[$route] = array(
                    'route' => $route,
                    'class' => $className,
                    'name'  => $instance->getModuleName()
                );
            }
            $this->setSessionValue('modules', serialize($retVal));
            return $retVal;
        }
    }

}