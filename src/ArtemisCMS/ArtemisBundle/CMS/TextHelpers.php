<?php
namespace ArtemisCMS\ArtemisBundle\CMS;

class TextHelpers {

    public static function prepareImageName($imageName, $newFileName = null) {
        if(!$newFileName)
            $newFileName = substr($imageName, 0, strrpos($imageName, "."));
        $ext = substr($imageName, strrpos($imageName, "."));
        return self::friendly_url($newFileName) . $ext;
    }


    /**
     * Create url from string
     * @static
     * @param $string
     * @return mixed|string
     */
    public static function friendly_url($string) {
        $url = $string;
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
        return $url;
    }
}