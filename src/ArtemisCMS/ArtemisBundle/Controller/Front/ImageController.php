<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Front;

use ArtemisCMS\ArtemisBundle\CMS\BaseController;
use ArtemisCMS\ArtemisBundle\CMS\Image;
use Symfony\Component\HttpFoundation\Response;


class ImageController extends BaseController
{

    public function defaultAction($profile_id, $id) {
        $Image = $this->getRepository('Image')->find($id);
        $ImageProfile = $this->getRepository('ImageProfile')->find($profile_id);
        $crop = ($ImageProfile->getCrop() ? true:false);
        $dir = CMS_IMAGE_PATH . "/$profile_id/";
        $image_path = $dir . "$id.png";

        if(!file_exists($dir))
            mkdir($dir, 0777, true);
        Image::resizeImage(CMS_IMAGE_PATH . "/original/" . $Image->getName(), $image_path, $ImageProfile->getWidth(), $ImageProfile->getHeight(), $crop,IMAGETYPE_PNG);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');
        $response->setContent(file_get_contents($image_path));
        return $response;
    }

}