<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Front;

use ArtemisCMS\ArtemisBundle\CMS\BaseController;

class ArticleController extends BaseController
{

    public function detailAction($slug) {
        return $this->renderFront('detail',array(
            'article' => $this->getRepository('Article')->findBySlug($slug)
        ));
    }

}