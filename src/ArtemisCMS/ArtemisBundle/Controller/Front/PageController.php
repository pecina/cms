<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Front;

use ArtemisCMS\ArtemisBundle\CMS\BaseController;


class PageController extends BaseController
{

    public function homepageAction() {
        $page = $this->getRepository('Page')->find(1);
        if($page->getTemplate())
            return $this->renderTemplate($page->getTemplate()->getId(), $this->fillParams($page));
        else
            return $this->renderFront('homepage', $this->fillParams($page));
    }


    public function defaultAction($slug) {
        $page = $this->getRepository('Page')->findBySlug($slug);
        if($page->getTemplate())
            return $this->renderTemplate($page->getTemplate()->getId(), $this->fillParams($page));
        else
            return $this->renderFront('default', $this->fillParams($page));
    }

    private function fillParams($page) {
        return array(
            'page' => $page,
            'contactform'   => $this->createContactForm()->createView()
        );
    }

    public function voteArticleAction($slug){
        $Article = $this->getRepository('Article')->findBySlug($slug);
        $Article->setVote($Article->getVote() + 1);
        $em = $this->getEm();
        $em->persist($Article);
        $em->flush();

        $User = $this->getUserByFacebook();
        if($User) {
            $Facebook = $User->getFacebook();
            $oldContent = $Facebook->getContent();
            if($oldContent)
                $oldContent.=", ";
            $Facebook->setContent($oldContent . 'Hlasoval pro web: ' . $Article->getTitle());
            $em = $this->getEm();
            $em->persist($Facebook);
            $em->flush();
        }

        return new \Symfony\Component\HttpFoundation\Response('OK');
    }


    public function addSiteAction() {
        return $this->renderFront('add-site');
    }


    public function createContactForm() {
        $form = $this->createFormBuilder()
            ->add('name', 'text')
            ->add('email', 'email')
            ->add('text', 'textarea');
        return $form->getForm();
    }


    public function sendFormAction() {
        $form = $this->createContactForm();
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $data = $form->getData();
            $message = \Swift_Message::newInstance()
                ->setSubject('Dotaz z webu od:' . $data['name'] )
                ->setFrom($data['email'])
                ->setTo($this->getConfigValue('contact_email'))
                ->setBody($data['text']);
            $this->get('mailer')->send($message);
        }
        return $this->redirect($this->getRequest()->headers->get('referer'));
    }


    public function addSiteComfirmAction() {
        $url = $this->requestGetParam('url');
        if($url) {
            $slug = \ArtemisCMS\ArtemisBundle\CMS\TextHelpers::friendly_url($url);
            $Art = $this->getRepository('Article')->findBySlug($slug);
            if($Art) {
                return $this->renderFront('exist');
            }
            $User = $this->getUserByFacebook();
            $Article = new \ArtemisCMS\ArtemisBundle\Entity\Article();
            $Article->setCreatedBy($User);
            $Article->setTitle($url);
            $Article->setSlug($slug);
            $Article->setPublished(true);
            $Article->addArticleCategory($this->getRepository('ArticleCategory')->findBySlug('stranky'));
            $em = $this->getEm();
            $em->persist($Article);
            $em->flush();

            // store add web
            $Facebook = $User->getFacebook();
            $oldContent = $Facebook->getContent();
            if($oldContent)
                $oldContent.=", ";
            $Facebook->setContent($oldContent . 'Přidal web: ' . $url);
            $em->persist($Facebook);
            $em->flush();

            $fb = $this->getFacebook();
            //publishStream($message, $name, $caption, $description = "", $link = "", $picture = "" )
            $fb->publishStream(
                'Zapojil som sa do súťaže.',
                'Nejhoršia stránka slovenského internetu',
                'Hladame  najhoršiu stránku',
                'Isto ste sa stretli počas surfovania  na vodách internetu s webmi, ktoré nemali ani hlavu ani pätu. Aj my. Keďže Google je naše ihrisko a internet náš svet, rozhodli sme sa zlepšiť jeho slovenskú úroveň. Pripravili sme súťaž do ktorej vy, používatelia internetovej siete môžete zapojiť web, ktorý je podľa vás najhorší rému môžeme pomôcť.',
                'http://' . $_SERVER['SERVER_NAME'] . $this->generateUrl('article', array(
                    'slug'  => $Article->getSlug()
                )),
                "http://api.thumbalizr.com/?url=$url&width=300&api_key=60158ec4040abbe9fca9618c0ce13bbe"
            );

            return $this->redirect($this->generateUrl('article', array(
                'slug'  => $Article->getSlug()
            )));
        }
    }

    /**
     * Get facebook login
     * @return \ArtemisCMS\ArtemisBundle\Entity\User|null
     */
    public function getUserByFacebook() {
        $facebook = $this->getFacebook();
        if($facebook->isLoged()) {
            $uid = $facebook->getUserId();
            $FacebookEn = $this->getRepository('Facebook')->findOneBy(array('uid' => $uid));
            if($FacebookEn) {
                $User = $this->getRepository('User')->findOneBy(array('facebook' => $FacebookEn->getId()));
                if($User)
                    $this->loginUser($User);

            }
            else {
                $userInfo = $facebook->getUserInfo();
                $User = new \ArtemisCMS\ArtemisBundle\Entity\User();
                $FacebookEn = new \ArtemisCMS\ArtemisBundle\Entity\Facebook();
                $FacebookEn->setUid($userInfo['id']);
                $em = $this->getEm();
                $em->persist($FacebookEn);
                $em->flush();
                $User->setFacebook($FacebookEn);
                $User->setEmail($userInfo['email']);
                $User->setFirstname($userInfo['first_name']);
                $User->setSurname($userInfo['last_name']);
                $User->setActive(true);
                $User->addRole($this->getRepository('Role')->findOneBy(array('name' => 'ROLE_REGISTRED')));
                $User->setUsername($userInfo['email']);
                $em->persist($User);
                $em->flush();
                $this->loginUser($User);
            }
        }
        return ($User?$User:null);
    }


    public function registerFacebookAction() {
        $this->getUserByFacebook();
        return $this->addSiteAction();
    }



}