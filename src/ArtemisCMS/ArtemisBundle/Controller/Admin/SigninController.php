<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;
use Symfony\Component\Security\Core\SecurityContext;

class SigninController extends BaseAdminController {

    /**
     * Sign in action, action view sign in form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function signinAction() {
        $request = $this->getRequest();
        $session = $request->getSession();

        // change locale
        $loc = $this->requestGetParam('locale');
        if(isset($loc) && $loc != null) {
            $this->get('session')->setLocale($loc);
            $this->setSessionValue('loginloc', $loc);
            //return $this->redirect($this->generateUrl('admin_signin'));
        }

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->renderAdmin('signin', array(
            // last username entered by the user
            'login'     => $session->get(SecurityContext::LAST_USERNAME),
            'error'     => $error
        ));
    }

    /**
     * Redirect after login
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction() {
        return $this->redirect($this->generateUrl('admin'));
    }
}