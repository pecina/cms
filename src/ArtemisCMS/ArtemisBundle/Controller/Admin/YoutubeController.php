<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseModule;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

/**
 * Create youtube video module for page
 * @author Pecina Ondřej
 */
class YoutubeController extends BaseModule
{

    /**
     * Render in page edit
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($page_id, $id)
    {
        return $this->renderAdmin('page', array(
            'form' => $this->createFormSetting($id)->createView(),
            'id' => $id
        ));
    }

    /**
     * Create form
     * @param $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormSetting($id) {
        $settings = $this->getSettings($id);
        if(!$settings)
            $settings = array();
        $form = $this->createFormBuilder($settings)
            ->add('link'.$id, 'text')
            ->add('width'.$id, 'integer')
            ->add('height'.$id, 'integer')
            ->add('fullscreen'.$id, 'choice', array(
                'required'  => false,
                'choices' => array(
                    '1' => $this->translate('ano'),
                    '0' => $this->translate('ne')
                )
            ))
            ;
        return $form->getForm();
    }

    /**
     * Render module
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function frontendAction($page_id, $id)
    {
        $settings = $this->getSettings($id);
        return $this->renderModule('frontend', array(
            'link' => $settings['link'.$id],
            'width' => $settings['width'.$id],
            'height' => $settings['height'.$id],
            'fullscreen' => $settings['fullscreen'.$id],
        ));
    }

    /**
     * Returns form name
     * @return string
     */
    public function getModuleName()
    {
        return 'Youtube video';
    }


}