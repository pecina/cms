<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;
use ArtemisCMS\ArtemisBundle\Entity\Page;
use ArtemisCMS\ArtemisBundle\Entity\Language;
use ArtemisCMS\ArtemisBundle\Entity\PageModule;

class PageController extends BaseAdminController {

    /**
     * Base action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  indexAction() {
        return $this->renderAdmin('index', array(
            'pages' => $this->getRepository('Page')->createTree(),
            'form'  => $this->createFormPage()->createView()
        ));
    }

    /**
     * Create page form
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormPage($id = null) {
        $page = new Page();
        $page->setCreatedBy($this->getLoggedUser());
        $hidden = null;
        if($id) {
            $page = $this->getRepository('Page')->find($id);
            if($id==1) $hidden = "hidden";
        }
        $form = $this->createFormBuilder($page)
            ->add('title')
            ->add('slug', $hidden)
            ->add('language')
            ->add('parent', $hidden)
            ->add('externalUrl', $hidden, array(
                'required' => false
            ))
            ->add('template');
        return $form->getForm();
    }

    /**
     * Create form
     * @return \Symfony\Component\Form\Form
     */
    public function createFormModules() {
        $choices = $this->getModules(__DIR__, __NAMESPACE__);
        $options = array();
        foreach( $choices as $choice )
            $options[$choice['route']] = $this->translate($choice['name']);
        $form = $this->createFormBuilder()
            ->add('module','choice', array(
                'choices' => $options
            ));
        return $form->getForm();
    }

    /**
     * Save page and render
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saveAction($id = null) {
        $form = $this->createFormPage($id);
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $data = $form->getData();
            $slugExists = false;
            // check existing slug
            $page = $this->getRepository('Page')->findBySlug($data->getSlug());
            if($page) {
                if(!$id || ($id && $id != $page->getId()))
                    $slugExists = true;
            }
            // store page
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($data);
            $em->flush();
            // proces existing slug
            if($slugExists) {
                $data->setSlug($data->getSlug() . "-" . $data->getId());
                $em->persist($data);
                $em->flush();
            }
            // redirect to edit
            return $this->redirect($this->generateUrl('admin_page_edit', array(
                'id' => $data->getId()
            )));
        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
        }
        if($id)
            return $this->renderEdit($id);
        else
            return $this->renderAdmin('index', array(
                'pages' => $this->getRepository('Page')->createTree(),
                'form'  => $this->createFormPage()->createView()
            ));
    }

    /**
     * Edit action
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id) {
        return $this->renderEdit($id);
    }

    /**
     * Render edit page action
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function renderEdit($id) {
        return $this->renderAdmin('edit', array(
            'pages'     => $this->getRepository('Page')->createTree(),
            'form'      => $this->createFormPage($id)->createView(),
            'moduleform'=> $this->createFormModules()->createView(),
            'page'      => $this->getRepository('Page')->find($id)
        ));
    }

    /**
     * Add module to page
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moduleAddAction($id) {

        $moduleForm = $this->createFormModules();
        $moduleForm->bindRequest($this->getRequest());
        if($moduleForm->isValid()) {
            $data = $moduleForm->getData();
            $modules = $this->getModules(__DIR__, __NAMESPACE__);
            $PageModule = new PageModule();
            $PageModule->setName($modules[$data['module']]['name']);
            $PageModule->setClass($data['module']);

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($PageModule);
            $page = $this->getRepository('Page')->find($id);
            $page->addPageModule($PageModule);
            $em->flush();

            // redirect to edit
            return $this->redirect($this->generateUrl('admin_page_edit', array(
                'id' => $id
            )));
        }
        else {
            foreach($moduleForm->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
            return $this->renderEdit($id);
        }
    }

    /**
     * Save module settings
     * @param $id
     * @param $module_id
     */
    public function moduleSettingsAction($id, $module_id) {
        $formValues = $this->requestGetParam('form');
        $em = $this->getDoctrine()->getEntityManager();
        // store new settings
        $PageModule = $this->getRepository('PageModule')
                            ->find($module_id);
        $PageModule->setSettings(serialize($formValues));
        // save into db
        $em->persist($PageModule);
        $em->flush();
        $this->renderEdit($id);
        return $this->renderEdit($id);
    }

    /**
     * Delete page
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id) {
        $Page = $this->getRepository('Page')
            ->find($id);
        if( $Page ) {
            $em = $this->getDoctrine()->getEntityManager();
            foreach($Page->getModules() as $Module)
                $em->remove($Module);
            $em->remove($Page);
            $em->flush();
        }
        else {
            $this->flashMessage('Záznam id nelze smazat protože neexistuje!');
        }
        return $this->indexAction();
    }

    /**
     * Delete module from page
     * @param $id
     * @param $module_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moduleDeleteAction($id, $module_id) {
        $PageModule = $this->getRepository('PageModule')->find($module_id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($PageModule);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_page_edit', array(
            'id'    => $id
        )));
    }

    /**
     * Resort module
     * @param $id
     * @param $module_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function moduleResortAction($module_id) {
        $position = $this->requestGetParam('position');
        $PageModule = $this->getRepository('PageModule')->find($module_id);
        $PageModule->setPosition($position);
        $em = $this->getEm();
        $em->persist($PageModule);
        $em->flush();
        return new \Symfony\Component\HttpFoundation\Response('OK', 200);
    }

}