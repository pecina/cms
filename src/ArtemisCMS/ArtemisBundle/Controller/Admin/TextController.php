<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseModule;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

/**
 * Create basic text module for page
 * @author Pecina Ondřej
 */
class TextController extends BaseModule
{

    /**
     * Render in page edit
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($page_id, $id)
    {
        return $this->renderAdmin('page', array(
            'form' => $this->createFormSetting($id)->createView(),
            'id' => $id
        ));
    }

    /**
     * Create form
     * @param $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormSetting($id) {
        $settings = $this->getSettings($id);
        if(!$settings)
            $settings = array();
        $form = $this->createFormBuilder($settings)
            ->add('text'.$id, 'ckeditor');
        return $form->getForm();
    }

    /**
     * Render module
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function frontendAction($page_id, $id)
    {
        $settings = $this->getSettings($id);
        return $this->renderModule('frontend', array(
            'text' => $settings['text'.$id]
        ));
    }

    /**
     * Returns form name
     * @return string
     */
    public function getModuleName()
    {
        return 'Textové pole';
    }


}