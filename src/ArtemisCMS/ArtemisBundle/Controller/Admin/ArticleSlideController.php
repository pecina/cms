<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseModule;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

/**
 * Page module article slide
 * @author Pecina Ondřej
 */
class ArticleSlideController extends BaseModule
{

    /**
     * Render in page edit
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($page_id, $id)
    {
        return $this->renderAdmin('page', array(
            'form' => $this->createFormSetting($id)->createView(),
            'id' => $id
        ));
    }

    /**
     * Create form
     * @param $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormSetting($id) {
        $settings = $this->getSettings($id);
        if(!$settings)
            $settings = array();

        $Categories = $this->getRepository('ArticleCategory')->findAll();
        $catVals = array();
        foreach($Categories as $category) {
            $catVals[$category->getId()] = $category->getName();
        }
        $EProfiles = $this->getRepository('ImageProfile')->findActive();
        $profiles = array();
        foreach($EProfiles as $profile) {
            $profiles[$profile->getId()] = $profile->getName();
        }

        $form = $this->createFormBuilder($settings)
            ->add('category_id', 'choice', array(
            'required'  => true,
            'multiple'  => true,
            'choices'   => $catVals
        ))
            ->add('orderBy','choice', array(
            'required'  => true,
            'choices'   => array(
                'createdAt' => $this->translate('data'),
                'id' => $this->translate('ID'),
                'title' => $this->translate('názvu'),
            )
        ))
            ->add('orderType','choice', array(
            'required'  => true,
            'choices'   => array(
                'asc' => $this->translate('vzestupně'),
                'desc' => $this->translate('sestupně'),
            )
        ))
        ->add('profile_id', 'choice', array(
            'required'  => true,
            'choices'   => $profiles
        ))
        ;
        return $form->getForm();
    }

    /**
     * Render module
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function frontendAction($page_id, $id)
    {
        $settings = $this->getSettings($id);
        $offset = (int) (isset($_REQUEST['offsetSlide'])?$_REQUEST['offsetSlide']:0);
        if(!$offset || !is_int($offset))
            $offset = 0;
        if (!$settings)
            return "";
        // find actual articles
        if (isset($settings['category_id']) && $settings['category_id'])
            $article = $this->getRepository('Article')
                ->findOneSlide($settings['category_id'], $settings['orderBy'], $settings['orderType'], $offset);
        // count of articles
        $count = $this->getRepository('Article')
            ->countOfArticles($settings['category_id'], $settings['orderBy'], $settings['orderType']);
        // render
        return $this->renderModule('frontend', array(
            'article'       => $article,
            'profile_id'    => (!isset($settings['profile_id'])?1:$settings['profile_id']),
            'offsetSlide'   => $offset,
            'count'         => $count
        ));
    }

    /**
     * Returns form name
     * @return string
     */
    public function getModuleName()
    {
        return 'Články - slide';
    }


}