<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;
use ArtemisCMS\ArtemisBundle\Entity\User;
class UsersController extends BaseAdminController {

    /**
     * Display existing users
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  indexAction() {

        // find limit
        $limit = $this->getConfigValue('items_page_limit');
        if(!$limit)
            $limit = 10;
        // find off set
        $offSet = (int)(isset($_REQUEST['offset'])? $_REQUEST['offset']:0);

        // find order by
        $orderby =  (isset($_REQUEST['orderby'])? $_REQUEST['orderby']:'id');

        // find order type
        $ordertype = (isset($_REQUEST['ordertype'])? $_REQUEST['ordertype']:'asc');


        return $this->renderAdmin('index', array(
            'users'     => $this->getRepository('User')->findAllItems($limit, $offSet, $orderby, $ordertype),
            'passform'  => $this->createFormPassword()->createView(),
            'limit'     =>  $limit,
            'offset'    =>  $offSet,
            'count'     =>  $this->getRepository('User')->findAllItemsCount(),
            'orderby'   =>  $orderby,
            'ordertype'   =>  $ordertype,
        ));
    }


    /**
     * Display existing users
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  facebookAction() {
        // find limit
        $limit = $this->getConfigValue('items_page_limit');
        if(!$limit)
            $limit = 10;
        // find off set
        $offSet = (int)(isset($_REQUEST['offset'])? $_REQUEST['offset']:0);

        // find order by
        $orderby =  (isset($_REQUEST['orderby'])? $_REQUEST['orderby']:'id');

        // find order type
        $ordertype = (isset($_REQUEST['ordertype'])? $_REQUEST['ordertype']:'asc');


        return $this->renderAdmin('facebook', array(
            'users'     => $this->getRepository('User')->findAllItemsFb($limit, $offSet, $orderby, $ordertype),
            'limit'     =>  $limit,
            'offset'    =>  $offSet,
            'count'     =>  $this->getRepository('User')->findAllItemsCountFb(),
            'orderby'   =>  $orderby,
            'ordertype'   =>  $ordertype,
        ));
    }

    /**
     * Create action create
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  createAction() {
        return $this->renderAdmin('create', array(
            'form' => $this->createFormUsers()->createView()
        ));
    }

    /**
     * Create action edit
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  editAction($id) {
        return $this->renderAdmin('edit', array(
            'form' => $this->createFormUsers($id)->createView(),
            'user'  => $this->getRepository('User')->find($id)
        ));
    }

    /**
     * Create users form
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormUsers($id = null) {
        $User = new User();
        if($id)
           $User = $this->getRepository('User')->find($id);
        $form = $this->createFormBuilder($User)
            ->add('username')
            ->add('firstname')
            ->add('surname')
            ->add('email')
            ->add('userRoles');
        if(!$id)
            $form->add('password');
        return $form->getForm();
    }

    public function deleteAction($id) {
        if($id && ($User = $this->getRepository('User')->find($id))) {
            $em = $this->getEm();
            $em->remove($User->getFacebook());
            $em->remove($User);
            $em->flush();
        }
        else {
            $this->flashMessage('Uživatele, který se pokoušíte smazat již neexistuje!');
        }
        return $this->redirect($this->generateUrl('admin_users'));
    }

    /**
     * Save action
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction($id = null) {
        $form = $this->createFormUsers($id);
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $User = $form->getData();
            $em = $this->getEm();
            $User = $this->convertUserPassword($User, $User->getPassword());
            $em->persist($User);
            $em->flush();
            if($id)
                $this->flashMessage("Uživatel byl úspěšně změněn.", FlashMessage::TYPE_INFO);
            else
                $this->flashMessage("Uživatel byl úspěšně vložen.", FlashMessage::TYPE_INFO);
        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
            if($id)
                return $this->renderAdmin('edit', array(
                    'form'  => $form->createView(),
                    'user'  => $this->getRepository('User')->find($id)
                ));
            else
                return $this->renderAdmin('create', array(
                    'form' => $form->createView()
                ));

        }
        return $this->redirect($this->generateUrl('admin_users'));
    }


    public function createFormPassword() {
        $form = $this->createFormBuilder()
            ->add('password', 'text')
            ->add('id', 'hidden');
        return $form->getForm();
    }

    /**
     * Change user password action
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changePasswordAction() {
        $form = $this->createFormPassword();
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $data = $form->getData();
            $User = $this->getRepository('User')->find($data['id']);
            if($User) {
                $User = $this->convertUserPassword($User, $data['password']);
                $em = $this->getEm();
                $em->persist($User);
                $em->flush();
                $this->flashMessage("Heslo bylo úspěšně změněno!", FlashMessage::TYPE_INFO);
            }
            else {
                $this->flashMessage("Uživatel nenalezen!", FlashMessage::TYPE_ERR);
            }
        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
        }
        return $this->redirect($this->generateUrl('admin_users'));
    }


    /**
     * Hash user password
     * @param \ArtemisCMS\ArtemisBundle\Entity\User $User
     * @param $password
     */
    private function convertUserPassword(User $User, $password) {
        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($User);

        $password = $encoder->encodePassword($password, $User->getSalt());
        $User->setPassword($password);
        return $User;
    }


}