<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\Entity\StaticBox;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

class ExtensionsController extends BaseAdminController {

    public function dashboardAction() {
        return $this->renderAdmin('dashboard');
    }


    /**
     * View for list of static boxes
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function staticboxesAction() {
        return $this->renderAdmin('staticboxes', array(
            'staticboxes' => $this->getRepository('StaticBox')->findAll()
        ));
    }

    /**
     * Create action for create repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function staticboxesCreateAction() {
        return $this->renderAdmin('staticboxes-create', array(
            'form' => $this->createFormStaticBoxes()->createView()
        ));
    }

    /**
     * Create action for create repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function staticboxesEditAction($id) {
        return $this->renderAdmin('staticboxes-edit', array(
            'form'      => $this->createFormStaticBoxes($id)->createView(),
            'staticbox' => $this->getRepository('StaticBox')->find($id)
        ));
    }

    /**
     * Create static box editing box
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormStaticBoxes($id = null) {
        $StaticBox = new StaticBox();
        if($id)
            $StaticBox = $this->getRepository('StaticBox')->find($id);

        $form = $this->createFormBuilder($StaticBox)
            ->add('slug')
            ->add('content', 'ckeditor', array(
                'required'  => false
            ));
        return $form->getForm();
    }

    public function staticboxesDeleteAction($id) {
        if($id && ($StaticBox = $this->getRepository('StaticBox')->find($id))) {
            $em = $this->getEm();
            $em->remove($StaticBox);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_extensions_staticboxes'));
        }
        else {
            $this->flashMessage('Staticbox, který jste se pokusil smazat již neexistuje!');
            return $this->staticboxesAction();
        }
    }


    public function staticboxesSaveAction($id = null) {
        $form = $this->createFormStaticBoxes($id);
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $StaticBox = $form->getData();
            $em = $this->getEm();
            $em->persist($StaticBox);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_extensions_staticboxes'));
        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);

            if($id)
                return $this->renderAdmin('staticboxes-create', array(
                    'form'      => $form->createView(),
                    'staticbox' => $this->getRepository('StaticBox')->find($id)
                ));
            else
                return $this->renderAdmin('staticboxes-create', array(
                    'form' => $form->createView()
                ));
        }
    }


}