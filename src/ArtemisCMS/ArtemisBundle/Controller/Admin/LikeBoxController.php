<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseModule;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

/**
 * Page module like box
 * @author Pecina Ondřej
 */
class LikeBoxController extends BaseModule
{

    /**
     * Render in page edit
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($page_id, $id)
    {
        $app_id = $this->getConfigValue('app_facebook_id');
        if(empty($app_id))
            return $this->renderAdmin('error');

        return $this->renderAdmin('page', array(
            'form' => $this->createFormSetting($id)->createView(),
            'id' => $id
        ));
    }

    /**
     * Create form
     * @param $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormSetting($id) {
        $settings = $this->getSettings($id);
        if(!$settings)
            $settings = array();
        $form = $this->createFormBuilder($settings)
            ->add('link'.$id, 'url')
            ->add('width'.$id, 'integer')
            ->add('height'.$id, 'integer')
            ->add('border'.$id, 'text', array(
                'required'  => false
            ))
            ->add('faces'.$id, 'choice', array(
                'choices'   => array(
                    '0' => $this->translate('ne'),
                    '1' => $this->translate('ano'),
                )
            ))
            ->add('stream'.$id, 'choice', array(
                'choices'   => array(
                    '0' => $this->translate('ne'),
                    '1' => $this->translate('ano'),
                )
            ))
            ->add('header'.$id, 'choice', array(
                'choices'   => array(
                    '0' => $this->translate('ne'),
                    '1' => $this->translate('ano'),
                )
            ))
            ;
        return $form->getForm();
    }

    /**
     * Render module
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function frontendAction($page_id, $id)
    {
        $settings = $this->getSettings($id);
        if(!$settings)
            return "";
        return $this->renderModule('frontend', array(
            'link' => $settings['link'.$id],
            'width' => $settings['width'.$id],
            'height' => $settings['height'.$id],
            'faces' => $settings['faces'.$id],
            'stream' => $settings['stream'.$id],
            'header' => $settings['header'.$id],
            'border' => $settings['border'.$id],
        ));
    }

    /**
     * Returns form name
     * @return string
     */
    public function getModuleName()
    {
        return 'Facebook LikeBox';
    }


}