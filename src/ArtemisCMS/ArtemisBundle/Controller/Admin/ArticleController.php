<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseModule;
use ArtemisCMS\ArtemisBundle\Entity\ArticleCategory;
use ArtemisCMS\ArtemisBundle\Entity\Article;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

/**
 * Article module
 * <br>
 * This class is also module for Page
 * @author Pecina Ondřej
 */
class ArticleController extends BaseModule
{

    //********************
    //  Article Category
    //********************

    /**
     * List of categories
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction() {
        return $this->renderAdmin('category', array(
            'categories' => $this->getRepository('ArticleCategory')
                                    ->findAll()
        ));
    }

    /**
     * Create category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createCategoryAction() {
        return $this->renderAdmin('category-create', array(
            'form' => $this->createFormCategory()->createView()
        ));
    }

    /**
     * Edit category
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editCategoryAction($id) {
        return $this->renderAdmin('category-edit', array(
            'form' => $this->createFormCategory($id)->createView(),
            'category' => $this->getRepository('ArticleCategory')->find($id)
        ));
    }

    /**
     * Delete category
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteCategoryAction($id) {
        if($id && ($Category = $this->getRepository('ArticleCategory')->find($id))) {
            $em = $this->getEm();
            $em->remove($Category);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('admin_article_category'));
    }

    /**
     * Save Article category
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saveCategoryAction($id = null) {
        $form = $this->createFormCategory($id);
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $em = $this->getEm();
            $em->persist($form->getData());
            $em->flush();
        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
            if($id)
                return $this->renderAdmin('category-edit', array(
                    'form' => $form->createView(),
                    'category' => $this->getRepository('ArticleCategory')->find($id)
                ));
            else
                return $this->renderAdmin('category-create', array(
                    'form' => $form->createView()
                ));
        }
        return $this->redirect($this->generateUrl('admin_article_category'));
    }

    /**
     * Create Article Category form
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormCategory($id = null) {
        $ArticleCategory = new ArticleCategory();
        if($id) // if exist load existing category
            $ArticleCategory = $this->getRepository('ArticleCategory')->find($id);
        $form = $this->createFormBuilder($ArticleCategory)
            ->add('name')
            ->add('slug')
            ->add('language')
            ->add('description', 'ckeditor');
        return $form->getForm();
    }

    //*******************
    //      Articles
    //*******************

    /**
     * List of articles
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesAction() {
        // find limit
        $limit = $this->getConfigValue('items_page_limit');
        if(!$limit)
            $limit = 10;
        // find off set
        $offSet = (int)(isset($_REQUEST['offset'])? $_REQUEST['offset']:0);

        // find order by
        $orderby =  (isset($_REQUEST['orderby'])? $_REQUEST['orderby']:'id');

        // find order type
        $ordertype = (isset($_REQUEST['ordertype'])? $_REQUEST['ordertype']:'asc');

        return $this->renderAdmin('articles', array(
            'articles'  =>  $this->getRepository('Article')->findAllItems($limit, $offSet, $orderby, $ordertype),
            'form'      =>  $this->createFormMasterImage()->createView(),
            'limit'     =>  $limit,
            'offset'    =>  $offSet,
            'count'     =>  $this->getRepository('Article')->findAllItemsCount(),
            'orderby'   =>  $orderby,
            'ordertype'   =>  $ordertype,
        ));
    }

    /**
     * Create articles
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesCreateAction() {
        return $this->renderAdmin('articles-create', array(
            'form' => $this->createFormArticles()->createView()
        ));
    }

    /**
     * Render edit article action
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesEditAction($id) {
        return $this->renderAdmin('articles-edit', array(
            'form' => $this->createFormArticles($id)->createView(),
            'article' => $this->getRepository('Article')->find($id)
        ));
    }

    /**
     * Action delete article
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesDeleteAction($id) {
        if($id && ($Article = $this->getRepository('Article')->find($id))) {
            $em = $this->getEm();
            $em->remove($Article);
            $em->flush();
        }
        else {
            $this->flashMessage('Článek, který se pokoušíte smazat již neexistuje!');
        }
        return $this->articlesAction();
    }

    /**
     * Save action
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesSaveAction($id = null) {
        $form = $this->createFormArticles($id);
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $em = $this->getEm();
            $Article = $form->getData();

            $ArticleSlug = $this->getRepository('Article')->findBySlug($Article->getSlug());

            $Article->setCreatedBy($this->getLoggedUser());
            $em->persist($Article);
            $em->flush();

            // if duplicated slug
            if($ArticleSlug) {
                $Article->setSlug($Article->getSlug() . "-" . $Article->getId());
                $em->persist($Article);
                $em->flush();
            }

        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
            if($id)
                return $this->renderAdmin('articles-edit', array(
                    'form' => $form->createView(),
                    'article' => $this->getRepository('Article')->find($id)
                ));
            else
                return $this->renderAdmin('articles-create', array(
                    'form' => $form->createView()
                ));
        }
        return $this->articlesAction();
    }

    /**
     * Create form for create/edit article
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormArticles($id = null) {
        $Article = new Article();
        if($id)
            $Article = $this->getRepository('Article')->find($id);
        $form = $this->createFormBuilder($Article)
            ->add('title')
            ->add('slug')
            ->add('language')
            ->add('published', null, array(
                'required' => false
            ))
            ->add('perex', null, array(
                'required' => false
            ))
            ->add('categories', null, array(
                'required' => true
            ))
            ->add('content', 'ckeditor', array(
                'required' => false
            ));

        return $form->getForm();
    }

    /**
     * Form for adding master image to article
     * @return \Symfony\Component\Form\Form
     */
    public function createFormMasterImage() {
        $form = $this->createFormBuilder()
            ->add('file', 'file')
            ->add('id', 'hidden');
        return $form->getForm();
    }

    /**
     * Add master image to article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesMasterImageAction() {
        $form = $this->createFormMasterImage();
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $data = $form->getData();
            $file = $data['file'];
            $em = $this->getEm();
            $Article = $this->getRepository('Article')->find($data['id']);
            $Image = new \ArtemisCMS\ArtemisBundle\Entity\Image();
            $fileName = \ArtemisCMS\ArtemisBundle\CMS\TextHelpers::prepareImageName($file->getClientOriginalName());
            $Image->setName($fileName);
            $em->persist($Image);
            $em->flush();
            $Article->setMasterImage($Image);
            $file->move(CMS_IMAGE_PATH . "/original/", $fileName);
            $em->persist($Article);
            $em->flush();
        }
        return $this->articlesAction();
    }


    //********************
    //    Modules def
    //********************

    /**
     * Create configuration form for module on page
     * @param $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormSettings($id) {
        $settings = $this->getSettings($id);
        if(!$settings)
            $settings = array();

        $Categories = $this->getRepository('ArticleCategory')->findAll();
        $catVals = array();
        foreach($Categories as $category) {
            $catVals[$category->getId()] = $category->getName();
        }
        $EProfiles = $this->getRepository('ImageProfile')->findActive();
        $profiles = array();
        foreach($EProfiles as $profile) {
            $profiles[$profile->getId()] = $profile->getName();
        }

        $form = $this->createFormBuilder($settings)
            ->add('category_id', 'choice', array(
                'required'  => true,
                'multiple'  => true,
                'choices'   => $catVals
            ))
            ->add('orderBy','choice', array(
                'required'  => true,
                'choices'   => array(
                    'createdAt' => $this->translate('data'),
                    'id' => $this->translate('ID'),
                    'vote' => $this->translate('Hlasů/Hodnocení'),
                    'title' => $this->translate('názvu'),
                )
            ))
            ->add('orderType','choice', array(
            'required'  => true,
            'choices'   => array(
                    'asc' => $this->translate('vzestupně'),
                    'desc' => $this->translate('sestupně'),
                )
            ))
            ->add('limit', 'integer', array(
                'required'  => true,
                'data'      => 0,
            ))
            ->add('profile_id', 'choice', array(
                'required'  => true,
                'choices'   => $profiles
            ))
        ;
        return $form->getForm();
    }

    /**
     * Show settings form
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($page_id, $id) {
        return $this->renderAdmin('page',array(
            'form' => $this->createFormSettings($id)->createView()
        ));

    }

    /**
     * Render module Articles into page in frontend
     * Dont call directly from route
     * @param $page_id
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function frontendAction($page_id, $id)
    {
        // load module settings
        $settings = $this->getSettings($id);
        if (!$settings)
            $settings = array();

        // offset articles
        $offset = (int) (isset($_REQUEST['offsetArticle'])?$_REQUEST['offsetArticle']:0);
        if(!$offset || !is_int($offset))
            $offset = 0;

        // find articles
        $articles = array();
        if (isset($settings['category_id']) && $settings['category_id'])
            $articles = $this->getRepository('Article')
                ->findModule($settings['category_id'], $settings['orderBy'], $settings['orderType'], $settings['limit'],$offset);

        // count of articles
        $count = $this->getRepository('Article')
            ->countOfArticles($settings['category_id'], $settings['orderBy'], $settings['orderType']);

        // render articles
        return $this->renderModule('frontend', array(
            'articles'      => $articles,
            'profile_id'    => (!isset($settings['profile_id'])?1:$settings['profile_id']),
            'offsetArticle' => $offset,
            'limit'         => $settings['limit'],
            'count'         => $count
        ));
    }

    /**
     * Return module name, not translate here, translating is in template!!
     * @return string
     */
    public function getModuleName() {
        return "Články";
    }

}