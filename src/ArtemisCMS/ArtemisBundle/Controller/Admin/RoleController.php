<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;

class RoleController extends BaseAdminController {

    public function  indexAction() {
        return $this->renderAdmin('index', array(
            'roles' => $this->getRepository('Role')->findAll()
        ));
    }


}