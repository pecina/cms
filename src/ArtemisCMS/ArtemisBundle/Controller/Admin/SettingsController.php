<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;
use ArtemisCMS\ArtemisBundle\Entity\Template;
use ArtemisCMS\ArtemisBundle\Entity\ImageProfile;

class SettingsController extends BaseAdminController {

    /**
     * Show settings dashboard
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction() {
        return $this->renderAdmin('dashboard');
    }

    //************************
    //      CONFIGS
    //************************
    /**
     * Create config action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configAction() {
        return $this->renderAdmin('config', array(
            'group_seo' => $this->createFormConfig($this->getRepository('Config')->findEditable('seo'))->createView(),
            'group_other' => $this->createFormConfig($this->getRepository('Config')->findEditable('other'))->createView(),
            'group_showset' => $this->createFormConfig($this->getRepository('Config')->findEditable('showset'))->createView()
        ));
    }

    /**
     * @param $items
     * @return \Symfony\Component\Form\Form
     */
    public function createFormConfig($items) {
        $form = $this->createFormBuilder();
        foreach( $items as $item ) {
            $form->add( $item->getKeyName(),$item->getInpuType()->getInpuType(), array(
                'data' =>  $item->getValue(),
                'label' => $this->translate($item->getDescription()),
                'required' => false
            ));
        }
        $this->clearConfig();
        return $form->getForm();
    }

    /**
     * Store config records
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configSaveAction() {
        $values = $this->requestGetParam('form');
        $em = $this->getDoctrine()->getEntityManager();
        foreach( $values as $key => $value ) {
            if($key != '_token') {
                    $Config = $em->getRepository('ArtemisBundle:Config')->findByKey($key);
                    $Config->setValue($value);
                    $em->persist($Config);
            }
        }
        $em->flush();
        return $this->redirect($this->generateUrl('admin_settings_config'));
    }



    //************************
    //      TEMPLATES
    //************************
    /**
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormTemplate($id = null) {
        $template = new Template();
        if($id)
            $template = $this->getRepository('Template')->find($id);
        $form = $this->createFormBuilder($template)
            ->add('name')
            ->add('content', 'textarea');

        return $form->getForm();

    }

    /**
     * Listo of templates
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function templatesAction() {
        return $this->renderAdmin('templates', array(
            'templates' => $this->getRepository('Template')->findAll()
        ));
    }

    /**
     * Create template
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function templateCreateAction() {
        return $this->renderAdmin('template-create', array(
            'form' => $this->createFormTemplate()->createView()
        ));
    }

    /**
     * Edit template
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function templateEditAction($id) {
        return $this->renderAdmin('template-edit', array(
            'form'      => $this->createFormTemplate($id)->createView(),
            'template'  => $this->getRepository('Template')->find($id)
        ));
    }

    /**
     * Delete template
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function templateDeleteAction($id) {
        $template = $this->getRepository('Template')->find($id);
        if($template) {
            $em = $this->getDoctrine()
                ->getEntityManager();
            $em->remove($template);
            $em->flush();
            return $this->templatesAction();
        }
        else {
            $this->flashMessage('Šablona, kterou se pokoušíte smazat neexistuje!');
            return $this->templatesAction();
        }
    }

    /**
     * Save template
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function templateSaveAction($id = null) {
        $form = $this->createFormTemplate($id);
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $em = $this->getDoctrine()
                ->getEntityManager();
            $Template = $form->getData();
            $em->persist($Template);
            $em->flush();
            $id = $Template->getId();

            // cache template

            $myFile = __DIR__ ."/../../Resources/views/Template/default.html.twig";
            $fh = fopen($myFile, 'r');
            $baseTemplate = fread($fh, filesize($myFile));
            fclose($fh);

            $myFile = __DIR__ ."/../../Resources/views/Template/Page/" . $id .".html.twig";
            if(file_exists($myFile))
                unlink($myFile);
            $fh = fopen($myFile, 'w');
            if(!$fh)
                throw new \Exception('Nemůžu zapisovat do souboru šablony.');

            fwrite($fh, str_replace('#TEMPLATE_CONTENT#', $Template->getContent(), $baseTemplate));
            fclose($fh);

            return $this->redirect($this->generateUrl('admin_settings_templates'));
        }
        if(!$id)
            return $this->renderAdmin('template-create', array(
                'form' => $form
            ));
        else
            return $this->renderAdmin('template-edit', array(
                'form' => $form
            ));
    }

    //************************
    //      IMAGE PROFILES
    //************************

    /**
     * Image profiles
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function imageProfilesAction() {
        return $this->renderAdmin('image-profiles', array(
            'profiles' => $this->getRepository('ImageProfile')->findActive()
        ));
    }

    /**
     * Create profile action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function imageProfilesCreateAction() {
        return $this->renderAdmin('image-profile-create', array(
            'form' => $this->createFormProfile()->createView()
        ));
    }

    /**
     * Render action for edit profiles
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function imageProfilesEditAction($id) {
        return $this->renderAdmin('image-profile-edit', array(
            'form' => $this->createFormProfile($id)->createView(),
            'profile' => $this->getRepository('ImageProfile')->find($id)
        ));
    }

    /**
     * Create form for work with image profile
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function createFormProfile($id = null) {
        $template = new ImageProfile();
        if($id)
            $template = $this->getRepository('ImageProfile')->find($id);
        $form = $this->createFormBuilder($template)
            ->add('name')
            ->add('width')
            ->add('height')
            ->add('crop', null, array(
                'required' => false
            ));

        return $form->getForm();
    }


    /**
     * Create form for work with image profile
     * @param null $id
     * @return \Symfony\Component\Form\Form
     */
    public function imageProfilesSaveAction($id = null) {
        $form = $this->createFormProfile($id)
                        ->bindRequest($this->getRequest());

        if($form->isValid()) {
            $ImageProfile = $form->getData();
            $em = $this->getEm();
            $em->persist($ImageProfile);
            $em->flush();
        }
        else {
            if($id)
                return $this->renderAdmin('image-profile-edit', array(
                    'form' => $form
                ));
            else
                return $this->renderAdmin('image-profile-create', array(
                    'form' => $form
                ));
        }

        return $this->imageProfilesAction();
    }

    /**
     * Delete image profile action
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function imageProfilesDeleteAction($id) {
        $ImageProfile = $this->getRepository('ImageProfile')->find($id);
        $em = $this->getEm();
        $em->remove($ImageProfile);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_settings_profiles'));
    }



}