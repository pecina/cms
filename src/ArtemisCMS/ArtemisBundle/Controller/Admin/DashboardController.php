<?php
namespace ArtemisCMS\ArtemisBundle\Controller\Admin;

use ArtemisCMS\ArtemisBundle\CMS\BaseAdminController;
use ArtemisCMS\ArtemisBundle\CMS\FlashMessage;
use ArtemisCMS\ArtemisBundle\Entity\Article;
use ArtemisCMS\ArtemisBundle\CMS\TextHelpers;

class DashboardController extends BaseAdminController {

    /**
     * Dashboard action
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function  indexAction() {
        $loginLoc = $this->getSessionValue('loginloc');
        if($loginLoc) {
            $this->setSessionValue('loginloc', null);
            return $this->generateUrl('admin', array(
                    '_locale' => $loginLoc
            ));
        }


        return $this->renderAdmin('index', array(
            'artcileForm' => $this->createFormArticle()->createView()
        ));
    }

    /**
     * Redirect after login
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function  redirectAction() {
        $loginLoc = $this->getSessionValue('loginloc');
        if($loginLoc) {
            $this->setSessionValue('loginloc', null);
            return $this->redirect($this->generateUrl('admin', array(
                '_locale' => $loginLoc
            )));
        }
        else {
            $this->setSessionValue('loginloc', null);
            return $this->redirect($this->generateUrl('admin', array(
                '_locale' => $this->get('session')->getLocale()
            )));
        }
    }


    /**
     * Create mini form for create article
     * @return \Symfony\Component\Form\Form
     */
    public function createFormArticle() {
        $Article = new Article();
        $form = $this->createFormBuilder($Article)
            ->add('title')
            ->add('content')
            ->add('categories');

        return $form->getForm();
    }

    /**
     * Create article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createArticleAction() {
        $form = $this->createFormArticle();
        $form->bindRequest($this->getRequest());
        if($form->isValid()) {
            $em = $this->getEm();
            $Article = $form->getData();
            $Article->setSlug(TextHelpers::friendly_url($Article->getTitle()));
            $Article->setCreatedBy($this->getLoggedUser());
            $Article->setPublished(true);
            $ArticleSlug = $this->getRepository('Article')->findBySlug($Article->getSlug());
            $em->persist($Article);
            $em->flush();

            // if duplicated slug
            if($ArticleSlug) {
                $Article->setSlug($Article->getSlug() . "-" . $Article->getId());
                $em->persist($Article);
                $em->flush();
            }
            $this->flashMessage('Článek byl úspěšně vytvořen a publikován!');
            return $this->redirect($this->generateUrl('admin'));
        }
        else {
            foreach($form->getErrors() as $error)
                $this->flashMessage($error->getMessageTemplate(), FlashMessage::TYPE_ERR);
            return $this->renderAdmin('index', array(
                'artcileForm' => $form->createView()
            ));
        }
    }
}