<?php

namespace ArtemisCMS\ArtemisBundle\Twig\Extension;

use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Base Artemis CMS Twig extension
 * <br>
 * Here can be defined own twig function
 * @author Pecina Ondřej
 * @version 1.0
 */
class BaseArtemisExtension extends \Twig_Extension
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * Database connection
     * @var \Doctrine\DBAL\Connection
     */
    private $conn;

    /**
     * Session
     * @var \Symfony\Component\HttpFoundation\Session
     */
    private $session;

    /**
     * Facebook service
     * @var \ArtemisCMS\ArtemisBundle\CMS\Facebook
     */
    private $facebook;

    /**
     * @var \Twig_Environment
     */
    private $environment;


    private $container;

    /**
     * Base Artemis CMS Twig extension
     * <br>
     * Here can be defined own twig function
     * <br>
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Symfony\Component\HttpFoundation\Session $session
     */
    public function __construct(\Doctrine\ORM\EntityManager $em, \Symfony\Component\HttpFoundation\Session $session, \ArtemisCMS\ArtemisBundle\CMS\Facebook $facebook, $container){
        $this->em = $em;
        $this->conn = $em->getConnection();
        $this->session = $session;
        if($this->cofigValue('app_facebook_id')) {
            $facebook->load($this->cofigValue('app_facebook_id'), $this->cofigValue('app_facebook_secret'));
            $this->facebook = $facebook;
        }
        $this->container = $container;
    }

    public function setContainer($container) {
        $this->container = $container;
    }


    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return  array(
            'totime' => new \Twig_Function_Method($this, 'toTime'),
            'dump' => new \Twig_Function_Method($this, 'dump'),
            'sqltest' => new \Twig_Function_Method($this, 'sqlTest'),
            'langname' => new \Twig_Function_Method($this, 'langName'),
            'configvalue' => new \Twig_Function_Method($this, 'cofigValue'),
            'is_facebook_logged' => new \Twig_Function_Method($this, 'isFacebookLogged'),
            'facebook_login_url' => new \Twig_Function_Method($this, 'getFacebookLoginUrl'),
            'facebook_logout_url' => new \Twig_Function_Method($this, 'getFacebookLogoutUrl'),
            'facebook_user_id' => new \Twig_Function_Method($this, 'getFacebookId'),
            'render_component' => new \Twig_Function_Method($this, 'renderComponent'),
            'static_box' => new \Twig_Function_Method($this, 'staticBox'),
            'ceil' => new \Twig_Function_Method($this, 'ceilValue'),
        );
    }

    public function renderComponent() {
        $args = func_get_args();
        if(count($args) <= 0)
            throw new \Exception('Minimal num of args = 1');
        $class = $args[0];
        // load existing components
        $components = $this->getComponents();
        // clear component name
        unset($args[0]);
        // render component
        $Component = new $components[$class]['class'];
        $Component->setContainer($this->container);
        return $Component->renderAction();
    }


    public function getComponents() {
        $ns = "ArtemisCMS\ArtemisBundle\Controller\Component";
        $dir = __DIR__ . "/../../Controller/Component";
        $components = $this->session->get('components');
        if($components)
            return unserialize($components);
        else {
            $classes = array();
            $handle = opendir($dir);
            while (false !== ($entry = readdir($handle))) {
                $classes[] = str_replace(".php","",$entry);
            }
            $classes = array_filter($classes, function ($class) {
                return strpos($class, 'Controller') !== FALSE;
            });
            foreach($classes as &$class) {
                $class=$ns."\\".$class;
            }
            // check module
            $classes = array_filter($classes, function ($class) {
                $classIns = new $class;
                if($classIns instanceof \ArtemisCMS\ArtemisBundle\CMS\BaseComponent)
                    return true;
                else return false;
            });
            $retVal = array();
            foreach($classes as $class) {
                $route = str_replace("ArtemisCMS\\","",str_replace("Controller","",str_replace("\\Controller\\",":",$class)));
                $retVal[str_replace("Controller", "", str_replace("$ns\\","",$class))] = array(
                    'route' => $route,
                    'class' => $class,
                );
            }
            $this->session->set('components', serialize($retVal));
            return $retVal;
        }
    }

    /**
     * Round value
     * @param $val
     * @return float
     */
    public function ceilValue($val) {
        return ceil($val);
    }

    /**
     * Static box
     * @param $ident
     * @return string
     */
    public function staticBox($ident) {

        if(is_int($ident)) {
            $StaticBox = $this->em->getRepository('ArtemisBundle:StaticBox')->find($ident);
        }
        else {
            $StaticBox = $this->em->getRepository('ArtemisBundle:StaticBox')->findBySlug($ident);
        }
        if($StaticBox)
            return $StaticBox->getContent();
        return "";
    }


    /**
     * Get config value
     * @param $key - key
     * @return null
     */
    public function cofigValue($key) {
        $values = unserialize($this->session->get('configvalues'));
        if(!$values) {
            $values = $this->em->getRepository('ArtemisBundle:Config')->findAll();
            $this->session->set('configvalues', serialize($values));
        }
        if(!is_array($values)){
            return null;
        }
        if(isset($values[$key]))
            return $values[$key]->getValue();
                return null;
    }


    /**
     * Converts a string to time
     *
     * @param string $string
     * @return int
     */
    public function toTime ($string)
    {
        return strtotime($string);
    }


    public function langName($lang) {
        return \ArtemisCMS\ArtemisBundle\CMS\Languages::$list[$lang];
    }

    /**
     * Dump variable
     * @param $variable
     * @return mixed
     */
    public function dump($variable) {
        return print_r($variable);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'base_artemis';
    }




    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }


    //******************************
    //          FACEBOOOK
    //******************************

    public function isFacebookLogged() {
        if(!$this->facebook)
            return "";
        return $this->facebook->isLoged();
    }

    public function getFacebookLoginUrl($scopes, $url) {
        if(!$this->facebook)
            return "";
        return $this->facebook->getLoginUrl($scopes,$url);
    }

    public function getFacebookLogoutUrl($url) {
        if(!$this->facebook)
            return "";
            
        return $this->facebook->getLogoutUrl($url);
    }

    public function getFacebookId() {
        if(!$this->facebook)
            return "";
        return $this->facebook->getUserId();
    }
}