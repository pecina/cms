<?php
/**
 * Remove dir recurse
 * @param $dir
 */
function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir."/".$object) == "dir") @rrmdir($dir."/".$object); else @unlink($dir."/".$object);
            }
        }
        reset($objects);
        @rmdir($dir);
    }
}

function chmod_R($path, $filemode, $dirmode) {
    if (is_dir($path) ) {
        if (!chmod($path, $dirmode)) {
            $dirmode_str=decoct($dirmode);
            print "Failed applying filemode '$dirmode_str' on directory '$path'\n";
            print "  `-> the directory '$path' will be skipped from recursive chmod\n";
            return;
        }
        $dh = opendir($path);
        while (($file = readdir($dh)) !== false) {
            if($file != '.' && $file != '..') {  // skip self and parent pointing directories
                $fullpath = $path.'/'.$file;
                chmod_R($fullpath, $filemode,$dirmode);
            }
        }
        closedir($dh);
    } else {
        if (is_link($path)) {
            print "link '$path' is skipped\n";
            return;
        }
        if (!chmod($path, $filemode)) {
            $filemode_str=decoct($filemode);
            print "Failed applying filemode '$filemode_str' on file '$path'\n";
            return;
        }
    }
}


//********************
// CACHE
//********************
// remove cache development
$src =  'app/cache/dev';
if(file_exists($src))
    rrmdir($src);
// remove cache production
$src =  'app/cache/prod';
if(file_exists($src))
    rrmdir($src);

chmod('app/cache/', 0777);

echo "Cache clear OK!\n";

//********************
// LOGS
//********************
$src =  'app/logs/dev.log';
if(file_exists($src))
    unlink($src);

$src =  'app/logs/prod.log';
if(file_exists($src))
    unlink($src);

chmod('app/logs/', 0777);

// Images chmod
chmod_R('web/images/', 0777,0777);

echo "Logs clear OK!\n";